<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    use HasFactory;
    public $video_url;
    protected $fillable = [
        'season',
        'episode',
        'title',
        'blurb',
        'show',
        'id',
        'video_url'
    ];
    public $timestamps = false;
    protected $hidden = [
        
    ];

    public function getVideoUrlAttribute($title = ""){

        if(!empty($this->attributes['video_url']))
        {
            return env('WAS_URL').$this->attributes['video_url'];

        }

        /** As i am make use of some formating tool for
         * files that i have editted this works with files  
        * that have episode listings in db          */
        $zero_buffer = '';
        if($this->episode < 10){
            $zero_buffer = '0';
        }
        return (env('WAS_URL').'television'.'/'.
                        $title.'/'.
                        'Season '.$this->season.'/'.
                        'S0'.$this->season.'E'.$zero_buffer.$this->episode.
                        '_'.str_replace(' ','_',$title).
                        '.mp4');
    }
}
