<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Actor;
use App\Models\Director;
use App\Models\Review;

use App\Models\Film_Actor;
use App\Models\Film_Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FilmExtraController extends Controller
{

    /**
      *      ..Why do you have a picture of a vacant lot in Paris Texas? "
      *   
      *                      "..because it's mine .."
     **/

    public $success;

    public static function show_extra_creator()
    {
        return view("film_view.forms.add_film_related_stuff");
    }

    public static function get_genres_from_film_id($id)
    {
        return DB::table('genres')
        ->join('film__genres', 'genres.id', '=','film__genres.genre')
        ->select('genres.*')
        ->where('film__genres.film', '=', $id )
        ->get();
    }

    public static function get_actors_from_film_id($id)
    {
        return DB::table('actors')
        ->join('film__actors', 'actors.id', '=','film__actors.actor')
        ->select('actors.*')
        ->where('film__actors.film', '=', $id )
        ->get();
    }

    public static function get_a_lew_review($id)
    {
        return DB::table('reviews')
        ->where('user', '=', '1' )
        ->where('movie',  '=', $id )
        ->first();
    }

    public static function create_anything_i_suppose(Request $request)
    {
        if(!empty($request->input("actor")))
        {
            $actor = new Actor(["name" => $request->input("actor")]);
            $actor->save();
        }
        if(!empty($request->input("director")))
        {
            $director = new Director;
            $director->name =  $request->input("director");
            $director->save();
        }

        if(!empty($request->input("genre")))
        {
            $genre = new Genre;
            $genre->name = $request->input("genre");
            $genre->save();
        }

        return FilmController::show_create_new_film($request);
    }

    public static function create_an_actor_film_association($actors, $id)
    {
        if($actors == null){ return;}
        foreach ($actors as $actor)
        {
            $actors_in_film = new Film_Actor;
            $actors_in_film->film = $id;
            $actors_in_film->actor = $actor;
            $actors_in_film->save();
        }
        FilmController::show_create_new_film();
    }

    public static function create_a_genre_film_association($genres, $id)
    {
        if($genres == null){ return;}

        foreach ($genres as $genre)
        {
            $actors_in_film = new Film_Genre;
            $actors_in_film->film = $id;
            $actors_in_film->genre = $genre;
            $actors_in_film->save();
        }
    }

    public static function create_review_for_film($film_id, $content, $user_id)
    {
        $review = Review::select('*')
        ->where('user', '=', $user_id)
        ->where('movie',  '=', $film_id )
        ->first();

        if($review == null){$review = new Review;}     
        
        $review->user = $user_id;
        $review->movie = $film_id;
        $review->content = $content;
        $review->save();
    }
}