<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Episode;
use Illuminate\Http\Request;


class TelevisionController extends Controller
{
    public function show_television_home(Request $request)
    {
        if($request->session()->get('status') != null){
            $request->session()->put('status','tv');
        }
        return view('film_view.home',array('telly'=> $this->get_recently_added(),
                                            'tv' => true));
    }

    public static function show_episode_of_show($title, $episodeId)
    {
        Episode::findOrFail($episodeId)->getVideoUrl($title);
        
    }

    public function show_television_listings(Request $request, $name)
    {
        return view('television.show',
                    array(
                        'id'   => $this->get_id_from_name($name),
                        'tvShow' => $this->get_details_about_show_from_name($name),
                        'data' => $this->get_season_information_for_show($name)
                    ));
    }

    public static function get_recently_added()
    {
        return  DB::table('televisions')
                    ->get();
    }

    public function get_season_information_for_show($showName)
    {   
        $episodes = Episode::join('televisions','televisions.id', '=','episodes.showID')
        ->where('episodes.showID',$this->get_id_from_name($showName))
        ->get();
        if(count($episodes) <= 0)
        {
            $bucketArr = BucketController::get_collection_episodes_from_tv($showName);
            return $bucketArr;
        }
        return $episodes;
    }

    public static function get_id_from_name($showName)
    {
       $episode =  DB::table('episodes')
        ->select('televisions.id')
        ->join('televisions','televisions.id', '=','episodes.showID')
        ->where('televisions.title', str_replace('_', ' ',$showName))
        ->first();
        if(isset($episode)){
            return $episode->id;
        }
        else{
            return DB::table('televisions')
            ->where('televisions.title', str_replace('_', ' ',$showName))
            ->first()->id;
        }
    }

    public function get_details_about_show_from_name($showName)
    {
        $tvShow = DB::table('televisions')
                    ->where('televisions.title', str_replace('_', ' ',$showName))
                    ->first();
        $tvShow->totalSeason = $this->get_total_seasons_from_name($showName);
        return $tvShow;
    }

    private function get_total_seasons_from_name($showName)
    {
        return DB::table('episodes')
        ->select(DB::raw('COUNT(DISTINCT season)'))
        ->join('televisions','televisions.id', '=','episodes.showID')
        ->where('episodes.showID',$this->get_id_from_name($showName))
        ->get();
    }
}
