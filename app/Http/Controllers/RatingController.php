<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use App\Models\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class RatingController extends Controller
{
    public static function get_film_ratings($filmID) 
    {

        return DB::table('ratings')->where('movie',$filmID)->get();
        
    }

    public static function get_my_rating($filmID)
    {
        return DB::table('ratings')->where('movie',$filmID)
                                   ->where('userID',2)
                                    ->get();
        
    }

    public static function create_film_review_from_star_rating(Request $request, $film_id)
    {

        create_film_rating($request->input("star"), $film_id);
        $film = Film::find(id: $film_id);
        return redirect('/film/'.$film->year.'/'.str_replace(' ','_',$film->title));
    }

    public static function create_film_rating($rating, $film_id)
    {
        $rating = new Rating;
        $rating->rating = $rating;
        $rating->film = $film_id;
        $rating->user = Auth::user()->id;
        $rating->save();
    }
}
