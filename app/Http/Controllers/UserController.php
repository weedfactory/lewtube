<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public static function ShowAccount()
    {
        
        /*  This assumes that the user is logged in to work. Because the Auth
            Middleware is checked with every route i can't see how it would fail.   */

            //if u say so ^
        return view('user.account', ["currentUser" => user::find(Auth::user()->id)]);
    }

    public function UpdateUserDetails(Request $request)
    {
        $currentUser = new User;
        
        $currentUser = User::find(Auth::user()->id);
        $currentUser->name = $request->name;
        $currentUser->email = $request->email;
        // CHANGING PASSWORD IS NOT ACTUALLY SET UP;

        $currentUser->save();
    }
}