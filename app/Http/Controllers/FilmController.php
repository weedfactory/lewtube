<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Director;
use App\Models\Actor;
use App\Models\Genre;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;




class  FilmController extends Controller
{
public $sucess;

    /**
     *       This is a snakeskin jacket! And for me it's a symbol of my individuality, 
     *                       and my belief... in personal freedom.
     */

        /* View Related Functions */
    public static function show_film_home()
    {
        return view('film_view.home', array(
                                        "films"      => FilmController::get_recent_films(),
                                        "film_count" => DB::table('films')->count('id')
                                        /*,"success"    => $sucess*/)
                                    );
    }

    public function show_all_films(){
        return view('film_view.search',array("data"=>    $this->get_all_films_sorted_by_director(),
                                        "success"=> $this->sucess));
    }

    public static function show_film_with_year_or_id($year, $film = null)
    {
        if($film == null)
        {
            $filmObj = Film::where('year',$year)->first();
        }
        else
        {
            $filmObj = Film::where('title',str_replace('_',' ',$film))->
            where('year',$year)->first();
        }
        // TODO: This could be refined, film object isn't used here.
        $id = $filmObj->id;
        
        /*      Film Information        */
        $data = DB::table('films')
                    ->join('directors', 'films.director', '=','directors.id')
                    ->select('films.*', 'directors.name')
                    ->where('films.id', '=', $id)
                    ->first();

        return view('film_view.show', array( "film" => $data,
                                        'genres'=>FilmExtraController::get_genres_from_film_id($id),
                                        'actors'=> FilmExtraController::get_actors_from_film_id($id),
                                        'lewReview'=>FilmExtraController::get_a_lew_review($id)));

    }

    public static function show_films_by_genre($genre)
    {
        return view('film_view.search',array("heading" => $genre ." Films",
                                    'data'=> FilmController::get_films_by_genre($genre)));
    }

    public static function show_films_by_actor($actor)
    {
        return view('film_view.search',array("heading" => $actor ." Films",
                                    'data'=> FilmController::get_films_by_actor($actor)));
    }

    public static function show_films_by_director($director)
    {

        return view('film_view.search', array("heading" => "Films Directed By ".$director,
                                        "data"    => FilmController::get_films_directed_by($director)));

    }

    public static function show_update_film_by_id(Film $film)
    {
            $directors  = Director::orderBy('name')->get();
            $actors     = Actor::all();
            $genres     = Genre::all();


            return view('film_view.update',[ 'film'  =>  $film,
                                    'actors' => $actors,
                                    'genres' => $genres,
                                    'directors' => $directors]);
    }

    public static function show_create_new_film()
    {
        $directors  = Director::orderBy('name')->get();
        $actors     = Actor::all();
        $genres     = Genre::all(); 


        return view('film_view.update',[ 
                                'actors' => $actors,
                                'genres' => $genres,
                                'directors' => $directors]);
    }
    /**         End Of View Related Functions */


    /**          Data Related Functions */    
    public static function get_recent_films()
    {
        return  DB::table('films')
            ->join('directors', 'films.director', '=','directors.id')
            ->select('films.*', 'directors.name')
            ->orderBy('films.id','DESC')
            ->limit(8)
            ->get();
    }

    private static function get_all_films_sorted_by_director()
    {
        return Film::select('films.*', 'directors.name')
                    ->join('directors', 'films.director', '=','directors.id')
                    ->orderBy('films.id','DESC')
                    ->get();
    }

    private static function get_films_directed_by($director)
    {
        $id = Director::where('name',$director)
        ->first()->id;

        /*      Finds Table of Filmography      */
        return DB::table('films')
                ->join('directors', 'films.director', '=','directors.id')
                ->select('films.*', 'directors.name')
                ->where('films.director', '=', $id )
                ->get();
    }



    /**
     * Takes a value of whatever genre and does something with it. 
     * @param mixed $genre
     * @return \Illuminate\Support\Collection
     */
    private static function get_films_by_genre($genre)
    {
        return DB::table('films')
                    ->join('film__genres', 'films.id', '=','film__genres.film')
                    ->join('genres','film__genres.genre','=','genres.id')
                    ->join('directors', 'films.director', '=','directors.id')
                    ->where('genres.name','=',$genre)
                    ->select('films.*', 'directors.name')
                    ->get();
    }

    private static function get_films_by_actor($genre)
    {
        return DB::table('films')
                    ->join('film__actors', 'films.id', '=','film__actors.film')
                    ->join('actors','film__actors.actor','=','actors.id')
                    ->join('directors', 'films.director', '=','directors.id')
                    ->where('actors.name','=',$genre)
                    ->select('films.*', 'directors.name')
                    ->get();
    }

    public function create_or_update_selected_film(Request $request)
    {
        $film = new Film;
        $film_id = $request->input('id');
        if($film_id != "")
        {
            $film->id = $film_id;
            $film->exists = true;
        }

        $request->validate([
            'title' => 'required',
            'about' => 'required',
            'year' => 'required',
            'director' => 'required'
        ]);

        $film->title = $request->input('title');
        $film->about = $request->input('about');
        $film->year = $request->input('year');
        $film->director = $request->input('director');

        $film->save();
        FilmExtraController::create_an_actor_film_association( $request->input('actor'), $film->id);
        FilmExtraController::create_a_genre_film_association( $request->input('genre'), $film->id);
        if( $request->input('review') != '')
        {
            FilmExtraController::create_review_for_film($film->id, $request->input('review'), Auth::user()->id);
        }
        
        $this->sucess = $film->title . " Successfully Updated";
        return FilmController::show_film_with_year_or_id($film->year, $film->title);
    }
    
    public function Delete(Film $film)
    {

        //TODO: auth user wrapper for delete
        /*  $film->delete();
        $this->sucess = $film->title . " Successfully Deleted";
        return FilmController::index();*/
    }
}
