<?php

namespace App\Http\Controllers;
use App\Http\Controllers\FilmController;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public static function bring_me_home(Request $request)
    {
        if($request->session()->get('status') != null){
            $request->session()->put('status','film');
        }
        $Film = new FilmController();
        return view('landing',array('films'=> $Film->get_recent_films()));
        
    }

    public static function LoadTFIGo(Request $request)
    {
        return view('tfi');
    }
}