<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{
    public static function GetFilmsuggestions($filmID){

        return DB::table('suggestions')->where('movie',$filmID)->get();
        
    }

    public static function GetMyreport($filmID){
        return DB::table('suggestions')->where('movie',$filmID)
                                   ->where('userID',2)
                                    ->get();
        
    }

    public static function SubmitRequest(Request $request, $filmID){
        $descript = $request->description_dropdown;
        if(empty($descript)){
            switch($request->description){
                case 'video':
                    $descript = 'video does not work';
                    break;
                case 'subtitles';
                    $descript = 'subtitles do not work';
                    break;
            }
        }
        $report = new Report;
        $report->description   = $request->description;
        $report->mediaID       = $filmID;
        $report->user          = Auth::user()->id;
        $report->type          = 'film';
        $report->save();
        $film = Film::find($filmID);
        return redirect('/film/'.$film->year.'/'.str_replace(' ','_',$film->title));
    }
}
