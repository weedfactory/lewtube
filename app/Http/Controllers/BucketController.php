<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
class BucketController extends Controller
{

    public static function get_collection_episodes_from_tv($name){
        $TvDirectory = new Collection();
        $season = 1;        // this should probably be done better but its my MF code.
        $show = "television/".str_replace('_',' ', $name);
        foreach(Storage::disk('Wasabi')->allDirectories($show) as $seasonDir)
        {
            $episode = 1;
            foreach (Storage::disk('Wasabi')->allFiles($seasonDir) as $dir)
            {
                $TvDirectory->push(new Episode(
                    [
                        'id'    => '540123',
                        'video_url' => $dir,
                        'season' => $season,
                        'episode'=> $episode,
                        'show'  => TelevisionController::get_id_from_name($name)
                    ]));
                $episode++;
            }
            $season++;
        }
        return $TvDirectory;
    }
}


?>