export class ApplicationState
{
    constructor()
    {
        this.buses_routes = [];
        this.active_tickets = [];
        this.available_tickets = [];
        this.favourite_tickets = [];
        this.history_tickets = [];

        this.user_name = "Alex Ryan";
        this.default_route;
    }
}