const canvas = document.getElementById('ticketCanvas');
const ctx = canvas.getContext('2d');

ctx.fillStyle = '#abdde4';
ctx.fillRect(0, 0, canvas.width, canvas.height);

// White circles in specific positions
const circles = [
    {x: 500, y: 30, radius: 12},
    {x: 500,  y: 90, radius: 12},
    {x: 500,  y: 150, radius: 12},
    {x: 500,  y: 210, radius: 12},
    {x: 500,  y: 270, radius: 12},
    {x: 0, y: canvas.height/2, radius: 60 },
    {x: canvas.width, y: canvas.height/2, radius: 60 }

];

ctx.fillStyle = 'white';
circles.forEach(circle => {
    ctx.beginPath();
    ctx.arc(circle.x, circle.y, circle.radius, 0, Math.PI * 2);
    ctx.fill();
});

// Text
ctx.fillStyle = 'black';

updateTicketInformation();

function updateTicketInformation()
{
    const ticket_zone = $(".zone > option:selected").text();
    const ticket_type = $(".type > option:selected").text();
    ctx.font = '30px Arial';
    ctx.fillText(ticket_zone, 90, canvas.height/2);
    ctx.font = '20px Arial';
    ctx.fillText('€ 4.20', 90, canvas.height/1.5);
    ctx.fillText(ticket_type, 30, canvas.height-40);
}

$(document).on("click", "select", function()
{
    updateTicketInformation();
});