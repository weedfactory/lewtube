$(function()
{
    const canvas = document.getElementById('canvas');
    const time_canvas =document.getElementById('time_canvas');
    const arrow_canvas = document.getElementById('arrow_canvas');
    const tfi_canvas = document.getElementById('tfi-logo');

    
    const ctx = canvas.getContext('2d');
    const arrow_ctx = arrow_canvas.getContext('2d');
    const time_ctx = time_canvas.getContext('2d');
    
    time_ctx.font = '50px Arial';
    time_ctx.textAlign = 'center';
    time_ctx.fillStyle = 'black';

    arrow_ctx.fillStyle =  '#35bdb1';    
    const arrows = [];
    const img = new Image();
    const arrowSize = 35;

    // Draw Circle Animation variables
    const centerX = canvas.width / 2;
    const centerY = canvas.height / 2;
    const baseOuterRadius = 75; 
    const baseMiddleRadius = 65; 
    const baseInnerRadius = 55; 
    const duration = 1500; 
    let angle = 0; // Rotation angle

    const points = [
        50, // Starting position
        50 + (600 - 100) * 0.3, // 30% to the right
        50 + (600 - 100) * 0.3 + (600 - 100) * 0.2 // 20% from the second point
    ];

    // Draw Bouncing Time variables
    var time_x = 200;
    var time_y = 80;
    var dx = -1;
    var dy = 1;

    arrow_init = true;
    let startTime;
    let paused = false; // Pause flag

    function drawPulsatingCircleAnimation(timestamp)
     {
        if (!startTime) startTime = timestamp;

        const elapsed = timestamp - startTime;
        
        // Calculate the scale factor for pulsating effect
        scale = ((Math.sin((elapsed / duration) * Math.PI * 2) + 1) / 2) + 1; 
        if(paused)
        {
            scale = 1.65;
        }

        ctx.clearRect(0, 0, canvas.width, canvas.height);        
        ctx.save();

        ctx.translate(centerX, centerY);
        ctx.rotate(angle);
        
        // Draws Outer circle
        ctx.fillStyle = '#00786b'; // Outer circle color
        ctx.beginPath();
        ctx.arc(0, 0, baseOuterRadius * scale, 0, Math.PI * 2);
        ctx.fill();
        
        // Draws Middle circle
        ctx.fillStyle = '#35bdb1';
        ctx.beginPath();
        ctx.arc(0, 0,  baseMiddleRadius * scale, 0, Math.PI * 2);
        ctx.fill();
        
        // Draw the inner circle
        ctx.fillStyle = '#00786b'; // Inner circle color
        ctx.beginPath();
        ctx.arc(0, 0, baseInnerRadius * scale, 0, Math.PI * 2);
        ctx.fill();
        
        // Draw the letter 'A'
        ctx.fillStyle = 'white'; 
        ctx.font = '100px Arial'; 
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText('A', 0, 0);
            
        // Restore the canvas state
        ctx.restore();
        
        // Update the rotation angle
        angle += Math.PI / 45; // Rotate a little bit for each frame

        if(!paused)
        {
            requestAnimationFrame(drawPulsatingCircleAnimation);
        }
    }

    function drawBouncingTimeAnimation()
    {
        time_ctx.clearRect(0, 0, time_canvas.width, time_canvas.height);
        time_ctx.beginPath();
        const date = new Date();
        const month = date.toLocaleString('default', { month: 'long' });
        minutes = date.getMinutes();
        if(minutes < 10)
        {
            minutes = "0"+minutes
        }
        current_date = date.getDate()
        if(current_date < 10)
        {
            current_date = "0"+current_date
        }
        const timeString = date.getHours() +":" + minutes + " " + current_date + " "+ month.substring(0, 3); 
        
        time_ctx.fillText(timeString, time_x, time_y); // Display the time at x, y
        time_ctx.closePath();
        time_x += dx; // Update x coordinate
        time_y += dy; // Update y coordinate

        // Bounce off the walls
        if (time_x + 150 > time_canvas.width || time_x - 150 < 0) {
            
            dx = -dx;
        }
        if (time_y + 40 > time_canvas.height || time_y - 40 < 0) {
            dy = -dy;
        }

        requestAnimationFrame(drawBouncingTimeAnimation);
    }

    function drawArrow(x, y, angle) {
        arrow_ctx.save();
        arrow_ctx.translate(x, y);
        arrow_ctx.rotate(angle * Math.PI / 360);
        arrow_ctx.drawImage(img, -arrowSize / 2, -arrowSize / 2, arrowSize, arrowSize);
        arrow_ctx.restore();
    }

    function drawRotatingArrowAnimation() {
        arrow_ctx.clearRect(0, 0, arrow_canvas.width, arrow_canvas.height);
        arrows.forEach(arrow => {
            arrow.angle += 5.6; // Adjust rotation speed here
            drawArrow(arrow.x, arrow.y, arrow.angle);
        });
        
        if(paused)
        {
            requestAnimationFrame(drawRotatingArrowAnimation);
            arrow_init = false;
        }
    }

    function drawRotatingArrows(x,y,angle) {

        img.src = 'tfi-go-fuck/assets/img_arrow.png';
        arrows.push({ x, y, angle });
        img.onload = () => {
            drawRotatingArrowAnimation();
        };
    }



    img.src = 'tfi-go-fuck/assets/img_arrow.png';

    drawRotatingArrows(arrow_canvas.width/10, arrow_canvas.height/4, 0);
    drawRotatingArrows(arrow_canvas.width/6, arrow_canvas.height/1.5, 190);
    drawRotatingArrows(arrow_canvas.width/1.2, arrow_canvas.height/4, -0);
    drawRotatingArrows(arrow_canvas.width/1.05, arrow_canvas.height/1.8, 190);
    drawRotatingArrows(arrow_canvas.width/1.2, arrow_canvas.height/1.3, -0);
    
    requestAnimationFrame(drawPulsatingCircleAnimation);
    requestAnimationFrame(drawBouncingTimeAnimation);
    requestAnimationFrame(drawRotatingArrowAnimation);
    $("canvas").on("click", function()
    {
        if(paused)
        {
            paused = false;
            requestAnimationFrame(drawPulsatingCircleAnimation);
            requestAnimationFrame(drawRotatingArrowAnimation);
            $(".canvas").css("background-color","white");
            img.src = "tfi-go-fuck/assets/img_arrow.png"

            return;
        }
        paused = true;
        $(".canvas").css("background-color","#35bdb1");
        img.src = "tfi-go-fuck/assets/img_arrow_white.png"
        angle = 0;
    })
});