<?php
require __DIR__.'/auth.php';

use App\Http\Controllers\FilmController;
use App\Http\Controllers\FilmExtraController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TelevisionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes            
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
|   routes are loaded by the RouteServiceProvider within a group which
|               contains the "web" middleware group.
|
*/


// Authentication Cover for Website
   // Route::middleware(['auth'])->group(function () {
     //   Route::get('film', [FilmController::class,'index'])->name("films");
     //   Route::get('television', [TelevisionController::class])->name("tv");
        Route::get('games', [FilmController::class])->name("games");
        Route::get('bucket/{name}',[TelevisionController::class, 'get_season_information_for_show'])->name("bucket");


    // Home Page 🤷
    Route::get('/', [HomeController::class, 'bring_me_home'])->name('home');

    // Routing For Films
    Route::prefix("film")->group(function()
    {
        Route::get("/edit/{film}",[FilmController::class, 'show_update_film_by_id'])->name('film.edit');
        Route::get("/delete/{film}",[FilmController::class, 'Delete'])->name('film.delete');
        
        
        Route::post("/update/{film}",[FilmController::class, 'create_or_update_selected_film'])->name( 'film.update');
        Route::get("/update/stuff",[FilmExtraController::class, 'create_anything_i_suppose'])->name( 'film.add_extras');
        Route::post("/update",[FilmController::class, 'create_or_update_selected_film'])->name('film.update');
        
        
        Route::post("/rating/{rating}",[RatingController::class, 'create_film_review_from_star_rating'])->name('film.rating');
        Route::post("/report/{film}",[ReportController::class, 'SubmitRequest'])->name('film.report');
        

        Route::get("/create",[FilmController::class, 'show_create_new_film'])->name('film.add');
        Route::get("/create/extras",[FilmExtraController::class, 'show_extra_creator'])->name('film.show_extras');
        
        Route::get('/recent',[FilmController::class, 'show_all_films'])->name('film.recent');
        

        Route::get("/director/{director}",[FilmController::class, 'show_films_by_director'])->name('film.director');
        Route::get("/actor/{actor}",[FilmController::class, 'show_films_by_actor'])->name('film.actor');
        Route::get("/genre/{genre}",[FilmController::class, 'show_films_by_genre'])->name('film.genre');
        
        // {Year}/{Film} can be anything so it must be last. 😮‍💨
        Route::get("{year}/{film}",[FilmController::class, 'show_film_with_year_or_id'])->name('film.show');
        Route::get("/{film}",[FilmController::class, 'show_film_with_year_or_id'])->name('film.showNoYear');
        Route::get("/",[FilmController::class, 'show_film_home'])->name('films');

    });                

    Route::prefix("tv")->group(function()
    {
        Route::get('/{name}', [TelevisionController::class, 'show_television_listings'])->name('tv.listing');
        Route::get('/{name}/{episodeId}',function ($name, $episodeId)
        {
            $videoUrl = TelevisionController::show_episode_of_show($name,$episodeId);
            return response()->json(['videoUrl' =>$videoUrl], 200, [], JSON_UNESCAPED_SLASHES);
        })->name('tv.show');
        Route::get('', [TelevisionController::class, 'show_television_home'])->name('tv.home');

    });
    Route::prefix("user")->group(function()
    {
        Route::get('watchlist',[UserController::class,'ShowAccount'])->name('user.watchlist');
        Route::get('update',[UserController::class,'ShowAccount'])->name('user.update');
        Route::get('account',[UserController::class,'ShowAccount'])->name('user.account');
        
    });

    Route::get("Free-FI", [HomeController::class, 'LoadTFIGo']);
    Route::get("free-fi", [HomeController::class, 'LoadTFIGo']);
    Route::get("tfi", [HomeController::class, 'LoadTFIGo']);
    Route::get("tfi-go", [HomeController::class, 'LoadTFIGo'])->name('tfi-go');

//});