@include('layouts.header')
<div class="register" style="margin-left: 10%; margin-top:5%">
    <link href="{{ asset('css/auth.css') }}" rel="stylesheet">
    <x-guest-layout>
            <x-slot name="logo">
                <a href="/">
                </a>
            </x-slot>

            <h2>Register</h2>
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <!-- Name -->
                <div class="mb-3">
                    <x-label class="" for="name" :value="__('Name')" />

                    <x-input class="form-control" id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                </div>

                <!-- Email Address -->
                <div class="mt-4">
                    <x-label for="email" :value="__('Email')" />

                    <x-input class="form-control" id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                </div>

                <!-- Password -->
                <div class="mt-4">
                    <x-label for="password" :value="__('Password')" />

                    <x-input id="password" class="block mt-1 w-full"
                                    type="password"
                                    name="password"
                                    required autocomplete="new-password"
                                    :value="old('password')" />
                </div>

                <!-- Confirm Password -->
                <div class="mt-4">
                    <x-label for="password_confirmation" :value="__('Confirm Password')" />

                    <x-input id="password_confirmation" class="block mt-1 w-full"
                                    type="password"
                                    name="password_confirmation" required />
                </div>

                <!-- Owner -->
                <div class="mt-4">
                    <x-label for="owner" :value="__('Owner')" />

                    <x-input class="form-control" id="email" class="block mt-1 w-full" type="name" name="owner" :value="old('owner')" required />
                </div>


                <div class="flex items-center justify-end mt-4">
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                        {{ __('Already registered?') }}
                    </a>

                    <x-button class="ml-4 btn btn-secondary">
                        {{ __('Register') }}
                    </x-button>
                </div>
            </form>
    </x-guest-layout>
</div>