@include('layouts.header')
<div class="container" style="margin-left: 10%; margin-top:10%;">
    <x-guest-layout>
            <x-slot name="logo">
                <a href="/">
                </a>
            </x-slot>
            <div class="mb-4 text-sm text-gray-600">
            <h2>Forgot your password?</h2>
            <br>Sorry homie, I don't have a host set up to send you an reset password email. But if you contact me i should be able to do it manually! (Or you can use a fake email and save the page)
            </div>

            <!-- Session Status -->
            <x-auth-session-status class="mb-4" :status="session('status')" />

            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
<!--
            <form method="POST" action="{{ route('password.email') }}">
                @csrf

               
                <div>
                    <x-label for="email" :value="__('Email')" />

                    <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                </div>

                <div class="flex items-center justify-end mt-4"><br>
                    <x-button class="btn-secondary" disabled>
                        {{ __('Email Password Reset Link') }}
                    </x-button>
                </div>
            </form>
-->
    </x-guest-layout>
</div>