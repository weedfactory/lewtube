@include('layouts.header')

<script>
            $(document).ready(function(){
                var selected = "About";
                $(".nav-item").click(function(){
                    var clicked = $(this).children().html();
                    if(selected != clicked){
                        $('.'+clicked).toggle();
                        $('.'+selected).toggle();
                        selected = clicked;
                        $(".active").removeClass('active');
                        $(this).children().addClass('active');

                    }
                });
                $(".stars__checkbox").click(function(){
                    let form = $(this).parents('form:first');
                    form.submit();
                });
                $('#ReportButton').click(function () {
                    $('#reportModal').modal('toggle');
                });
                $('.lewReviewBtn').click(function() {
                    $('.lewReview').toggle(250);
                });
            });
        </script>
<div class="container">   
    <div class="row">
        <video class="video video-js" controls crossorigin="anonymous" preload="metadata">
            <source src="https://s3.eu-central-1.wasabisys.com/lewtube/film/{{$film->name}}/{{$film->year}} - {{$film->title}}.mp4" type="video/mp4">
            <track src="https://s3.eu-central-1.wasabisys.com/lewtube/film/{{$film->name}}/{{$film->year}} - {{$film->title}}.vtt" kind="captions" label="english" default srclang="en">
        </video>
        <script type="module" src="{{ asset('js/video-player.js') }}"></script>

    </div>
    
<!-- Heading Content -->
    <div class="row">
        <div class="col-md-12" style="padding:1%">
                    <h1>
                        <a class="btn btn-light"  href="{{ route('films') }}"> Back</a>
                            {{$film->year}} - {{ $film->title }}
        

                    </h1>
        </div>
    </div>


    <!-- Body Content-->
    <div class="row">

    <!-- Poster -->
        <div class="col">
            <img style="width:100%; height:auto" src="https://s3.eu-central-1.wasabisys.com/lewtube/images/film/{{ $film->id }}.png">
        </div>


        <!-- Navigation Items For Film Details -->
        <div class="col">
            <ul class="nav nav-tabs" >
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Details</a>
                </li>
            </ul>

            <!-- Default Display -->
            <div class="row"style="padding-top:2%;">
                <div class="col-md-9 About" style="text-align: center;">{{ $film->about }}</div>
                <div class="col About">
                  <h4>Directed By:</h4><br>
                        <a class="btn btn-light" style="margin-left:5%" href="{{route('film.director', $film->name)}}">
                            {{ $film->name }}
                        </a>
                        @if(Auth::user() != null)
                        <hr>
                @if(Auth::user()->type == "ADMIN")
                    @auth
                        <a class="btn btn-light" href="{{route('film.edit',$film->id)}}">Edit</a>       
                        <a class="btn btn-danger" href="{{route('film.delete',$film->id)}}">Delete</a>      
                    @endauth
                @endif
            @endif                
                </div>

                @if($lewReview != null)
                    <div class="col-xl-9 row About" >
                        <button class="lewReviewBtn btn btn-light about" style="margin:5%"> Want to know what i think?</button>
                        <div class="lewReview about" style="display: none; text-align: center;"> {!! $lewReview->content !!}</div>
                    </div>
                @endif
            </div>
            <div class="row Details" style="display: none;">
                    <div class="col-xl-9" style="padding:2%">
                        <h4>Genre:</h4>
                        <br>
                        @foreach ($genres as $key => $genre)
                        <a class="btn btn-light" href="{{route('film.genre', $genre->name)}}">
                            {{ $genre->name }}
                        </a>
                        @endforeach
                    </div>

                    <div class="col-xl-9" style="padding:2%">
                     <h4>Actors:</h4>
                     <br>
                            @foreach ($actors as $key => $actor)
                            <a class="btn btn-light" href="{{route('film.actor', $actor->name)}}">
                                {{ $actor->name }}
                            </a>
                            @endforeach
                    </div>
                    @if(Auth::user() != null)
                        <div class="col-xl-9">
                        <h4>Rating</h4>
                        <x-auth-session-status class="mb-4" :status="session('status')" />
                            <!-- Validation Errors -->
                            <x-auth-validation-errors class="mb-4" :errors="$errors" />
                                    @include('film_view.forms.rating-form')
                        </div>
                    </div>
                    @endif
             </div>
        </div>

    </div>
  
        </div>
    </div>
</div>
@include('film_view.forms.modal_report')

@include('layouts.footer')

