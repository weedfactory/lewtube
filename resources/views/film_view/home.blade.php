<link rel="stylesheet" href="{{ asset('css/star-parallax.css') }}">
@include('layouts.header')


<div class="container">
<div class="twinkle twinkling" style="height:200%"></div>
    <div class="stars" style="height: 200%;" >
        </div>
        <div class="wrapper" style="height:50vh">
            <ul id="scene">
                <li class="layer" data-depth="0.00"></li>

                <li class="layer" id="menu" data-depth="0.30" style="z-index:20; padding: 20vh 25vw;">
                    <h2> I hope you are having a gr8 day😎 </h2>
                    @if(isset($film_count))
                    <h3 style="padding-left:5vw">I have 
                        <a href="film/recent">
                            <button class="total-film-star" style="background-image :url('images/star-gif.gif');" >
                                {{$film_count}} 
                            </button>
                        </a>
                        movies
                    </h3>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </li>

                <li class="layer" data-depth="0.60"><a href="tv"><p><img class="floaters" src="images/hypno-castle.gif"> </p></a></li>
                <li class="layer" data-depth="0.60"><a href="tv"><p> </p></a></li>
                <li class="layer" data-depth="0.60" style="padding-top:70vh"><a href="tv"><p><img class="floaters" src="images/welcome2lewtube.gif"> </p></a></li>
                <li class="layer" data-depth="0.60"><p></p></li>
                <li class="layer" data-depth="0.60"><a href="tv"><p><img class="floaters" src="images/iloveamberleaf2.gif"> </p></a></li>
                <li class="layer" data-depth="0.60"><a href="tv"><p><img class="floaters" src="images/skele.gif"> </p></a></li>
                <li class="layer" data-depth="0.60"><a href="tv"><p><img class="floaters" src="images/recentlyadded.gif"> </p></a></li>
                <li class="layer" data-depth="0.60"><a href="tv"><p><img class="floaters" src="images/sakura-dance.gif"> </p></a></li>


            </ul>

        </div>
        
        
        <div class="row">
        
            <div class="col-md-12" style="    background: linear-gradient(to top, rgba(0, 0, 0, 1), rgba(0, 0, 0, 0));">
                <!-- Recently Added Films -->
                @isset($films)
                <legend style="padding-left: 10%"><a href={{route('film.recent')}}>Check out all films that were recently added</a> </legend>

                <fieldset>
                        <div class="row">
                        @foreach ($films as $film )
                            <div class="col-xl-3 card">
                                <div class="card-body">
                                <a href="{{route('film.show',['year'=>  $film->year, 
                                                    'film' =>   str_replace(' ','_',$film->title)]) }}">
                                                    <img class="card-img-top" src="https://s3.eu-central-1.wasabisys.com/lewtube/images/film/{{ $film->id }}.png">

                                    <h5 class="card-title">{{$film->year}} - {{$film->title}}</h5>
                                </a>
                                    <p class="card-text" >{{$film->about}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
                @endisset
            <!-- END Recently Added Films -->
                @isset($telly)
                <legend>Recently Added Television Shows </legend>
                    <fieldset>
                            <div class="row">
                            @foreach ($telly as $show )
                                <div class="col-xl-3">
                                    <div class="card-body">
                                    <a href="{{route('tv.listing',['name' =>   str_replace(' ','_',$show->title)]) }}">
                                                        <img class="card-img-top" src="https://s3.eu-central-1.wasabisys.com/lewtube/images/television/{{ $show->id }}.jpg">

                                        <h5 class="card-title">{{$show->title}}</h5>
                                    </a>
                                        <p class="card-text" >{{$show->about}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                @endisset		
            </div>
        </div>
    </div>
</div>
<style>
    fieldset
    {
        z-index: 5;
        position: relative;
        max-height: 70%;
        overflow-y: scroll;
    }

</style>
@include('layouts.footer')
