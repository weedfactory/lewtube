@include('layouts.header')
<style>
.form-control:focus, .form-control
{
    background-color: white;
}
</style>
<div class="container" style="height: 100%">
    <nav class="nav justify-content-center">
                <a class="nav-link active" href="#">Film</a>
                <a class="nav-link" href="#">Update films</a>
    </nav>
    
    <div class="row">      
        @include('film_view.forms.film_controller')

    </div>

    <div class="row">
        @include('film_view.forms.add_film_related_stuff')
    </div>
</div>