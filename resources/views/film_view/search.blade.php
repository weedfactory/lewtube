<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<link href="{{ asset('css/filmTable.css') }}" rel="stylesheet">

@include('layouts.header')

<script>
    $(document).ready(function() {
    $('#filmTable').DataTable( {
        stateSave: true,    
    } );

    $('.filmTable').css("background-color","#333");
} );

</script>
@yield('content')

@if(isset($success))
    <div class="alert alert-success" role="alert">
        {!!$success!!}
    </div>
@endif
<div class="container" style="border-radius: 25px">

<div class="row">
    <h2>My Movies</h2>
    <p><strike>These are all films i have watched since making the website</strike> I have unfortunately lost a database containing several years of contributions i had made to this website by my own carelessness and impatiences, i am slowing working at adding it all back. If there are any films that you would like me to add or think i should like you can let me know!</p>
    <div class="col-xl-12" style="border-radius: 25px">
    <table class="filmTable table table-striped" id="filmTable" style="border-radius: 25px;">
        <thead>
            <tr>
                @if(isset($heading))
                <td class="alert alert-success" colspan="7">
                    {!!$heading!!}
                </td>
                @endif
            </tr>
            <tr>
                <td></td>
                <td>Title</td>
                <td>About</td>
                <td>Director</td>
                <td>Year</td>
                @if(Auth::user() != null)
                    @if(Auth::user()->type == "ADMIN")
                        <td></td>
                    @endif
                @endif
            </tr>
        </thead>
        <tbody>


        <!-- Film Ouput -->
        @foreach ($data as $key => $film)
        <a>
            <td>
                <a href="{{route('film.show',['year'=>  $film->year, 
                                            'film' =>   str_replace(' ','_',$film->title)]) }}">
                <img class="table-img" src="https://s3.eu-central-1.wasabisys.com/lewtube/images/thumbnails/{{ $film->id }}.jpg">
            </td>
            <td>
            <a href="{{route('film.show',['year'=>  $film->year, 
                                        'film' =>   str_replace(' ','_',$film->title)]) }}">
                {{ $film->title }}</td>
            </a>
            <td>
            <a href="{{route('film.show',['year'=>  $film->year, 
                                        'film' =>   str_replace(' ','_',$film->title)]) }}">
                {{ $film->about }}</td>
            </a>
            <td>
                <a class="btn btn-light" href="{{route('film.director', $film->name)}}">
                {{ $film->name }}
                </a>
            </td>
            <td>{{ $film->year }}</td>
            @if(Auth::user() != null)
                @if(Auth::user()->type == "ADMIN")
                <td>
                    @auth
                        <a class="btn btn-primary" href="{{route('film.edit',$film->id)}}">Edit</a>       
                        <a class="btn btn-danger" href="{{route('film.delete',$film->id)}}">Delete</a>      
                    @endauth
                </td>
                @endif
            @endif
        </tr>
                @endforeach
        </tbody>
    </table>
    </div>
</div>
                </div>



<!-- Data Tables Sheets -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

@include('layouts.footer')
