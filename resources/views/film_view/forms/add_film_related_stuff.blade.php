
<form method="get" class="form col-md-12" action="{{ route('film.add_extras') }}">
    
    @csrf
    <fieldset class="row" style="margin-top:5%" style="border-color:purple">
        <h2>Create New Film Related Stuff</h2>
        <div class="col-md-3">        
            <h3>Add Actor</h3>   
                <input  id="actor" class="block mt-1 w-full" type="text" name="actor" :value="old('actor')" />
        </div>
        
        <div class="col-md-3">        
            <h3>Add Genre</h3>   
            <input id="genre" class="block mt-1 w-full" type="text" name="genre" :value="old('genre')" />
        </div>
        
        <div class="col-md-3">        
            <h3>Add Director</h3>   
                <input  id="director" class="block mt-1 w-full" type="text" name="director" :value="old('director')" />
        </div>
        <button type="submit" class="btn btn-light" style="max-width:15%; max-height:25vh">Add This Stuff</button>
    </fieldset>

</form>