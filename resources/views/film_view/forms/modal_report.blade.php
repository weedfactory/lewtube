<style>
    .form-group{
        padding-bottom:5%;
        padding-top: 1%;
    }
    .description{
        margin-left: 25%;
        width: 50%;
    }
    .mb-3{
        color: indianred;
    }

</style>

<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header header">
                <h5 class="modal-title" id="reportModalLabel">Report An Issue</h5>
                <button type="button" class="close btn" id='ReportButton' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('film.report',$film->id)}}" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="description">Select one of these catch all issues.</label>
                    <select name="description" class="description">
                        <option value="Video">Video Doesn't Work</option>
                        <option value="Subtitles">Subtitles Don't Work</option>
                    </select>
                </div>    
                <h2 style='text-align:center'>- or - </h2>
                <div class="mb-3">
                    <label for="description-dropdown">Description</label><br>
                    <textarea name="description_dropdown" class="description" placeholder="please give details about the issue."></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id='ReportButton' data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-light">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>