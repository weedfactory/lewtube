<form method="POST" class="form col-md-12" action="{{ route('film.update', ) }}">
  <fieldset>          
    @csrf
    <h3>
        @if (isset($film->id))
            Updating Information for film with id #{{ $film->id }} - {{ $film->title }}   
        @else
            Create A New Film
        @endif
    </h3>

            <div class="col-md-6" style="float:left;">

                <input type="hidden" value={{$film->id ?? ""}} name="id">
                <!-- Title -->
                <label  for="title">Title</label>
                <input  id="name" class="form-control" type="text" name="title" value="{{$film->title ?? old('title')}}" required autofocus />
                
                <!-- About -->
                <label for="about" class="form-label" >About</label>
                <textarea id="about" class="form-control" type="textarea" name="about" required>{{$film->about ?? old('about') }}</textarea>
                
                <!-- Year -->
                <label for="year" class="form-label" >Year Of Release</label>
                        <input id="year" class="form-control" type="number" name="year" value="{{ $film->year ?? old('year') }}" required>


                <label for="review" class="form-label"> Review </label>
                    <textarea id="review" class="form-control" type="textarea" name="review"></textarea>
                
            </div> <!-- end of stuff -->
            <div class="col-md-6" style="float:right; padding-left: 5%">

                    <!-- Director -->
                        <label for="director">Director</label>
                    <select class="form-control" name='director'>
                            @foreach ($directors as $key => $director)
                                @if (isset($film->id))
                                        @if($director->id == $film->director)
                                                <option selected value="{!!$director->id!!}">{{$director->name}}</option>
                                        @endif
                                @endif
                                <option value="{!!$director->id!!}">{{$director->name}}</option>
                            @endforeach
                    </select>

                    <label for="actor">Actor</label>
                    <select class="form-control" name='actor[]' multiple >
                            @foreach ($actors as $key => $actor)
                                <option value="{!!$actor->id!!}">{{$actor->name}}</option>
                            @endforeach
                    </select>

                    <label for="genre">Genre</label>
                    <select class="form-control" name='genre[]' multiple >
                            @foreach ($genres as $key => $genre)
                                <option value="{!!$genre->id!!}">{{$genre->name}}</option>
                            @endforeach
                    </select>
            </div> <!-- end of foreign key stuff -->
        <div class="col-md-12">
            <div class="col" style="float:right">
                @if(isset($film->id))
                    <button type="submit" name="" id="" class="btn btn-light" style="margin-top: 10%">Update {{$film->title}}</button>
                @else
                    <button type="submit" name="" id="" class="btn btn-light" style="margin-top: 10%">Create New Film</button>
                @endif()
            </div>
        </div>
    </fieldset>
</form>