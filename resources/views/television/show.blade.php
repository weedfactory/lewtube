<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<link href="{{ asset('css/filmTable.css') }}" rel="stylesheet">

@include('layouts.header')

<style>

.episodes-list {
    display: flex;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
}

.episode-pair {
    display: flex;
    flex-basis: 50%;
    padding: 10px;
    box-sizing: border-box;
}


</style>
@yield('content')

@if(isset($success))
    <div class="alert alert-success" role="alert">
        {!!$success!!}
    </div>
@endif

<div class="container" style="border-radius: 25px">
<div class="cabinet col-xl-12" style="display: none;" >
        <h2 class="EpisodeTitle"></h2>
        <video id="TvDisplay" style="width:100%" controls>
            <source src="" type="video/mp4">
        </video>
    </div>
    <div class="row">



    <!-- Poster -->
    <div class="col-xl-3">
        <img style="width:100%; height:auto" src="https://s3.eu-central-1.wasabisys.com/lewtube/images/television/{{ $id }}.jpg">
    </div>
    
    <!-- Body -->
    <div class="col-xl-9">
        <h2>{{ $tvShow->title }}</h2>
            {{ $tvShow->about }}
            <br>

        
        <!-- Default Display -->
        <div class="row"style="padding-top:2%;">
			@foreach ($data->groupBy('season') as $seasonNumber => $seasonEpisodes)
            <h2>Season {{ $seasonNumber }}</h2>
            <hr>

<div class="row">
    @foreach ($seasonEpisodes as $episode)
        <div class="col-xl-6">
            <a href="#" class="play-episode" data-episode-id="{{ $episode->id }}" data-show-title="{{ $episode->title }}" data-video-url="{{$episode->getVideoUrlAttribute($episode->title)}}">
                <div class="episode-info" >
                    <strong>S{{$episode->season}} E{{ $episode->episode }}  {{ $episode->name }}</strong>
                </div>
            </a>
        </div>
    @endforeach
</div>
			@endforeach
        </div>
        <script>
        $(function() {
            // Show/hide the cabinet when the video is played/stopped
            var cabinet = $('.cabinet');
            var video = $('#TvDisplay');

            video.on('play', function() {
                cabinet.show();
            });



            // Change the video source when an episode is clicked
            $('.play-episode').click(function(event) {
                event.preventDefault();

                var episodeId = $(this).data('episode-id');
                var showTitle = $(this).data('show-title');
                var url = $(this).data('video-url');
                $('.EpisodeTitle').html = $(this).html;
                if(url == null){
                    url = '{{ route("tv.show", ["name"=> ":showTitle","episodeId" => ":episodeId"]) }}'
                    .replace(':episodeId', episodeId)
                    .replace(':showTitle', showTitle);
                }
                video.attr('src',url);
                $('.cabinet').slideDown(300);
                video[0].focus();
                
                
            });
        });
    </script>
    </div>

    </div>

    </div>
</div>



<!-- Data Tables Sheets -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

@include('layouts.footer')