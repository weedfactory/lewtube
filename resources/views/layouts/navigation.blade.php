<header>
    <nav class="navbar navbar-expand-md navbar-fixed-top navbar-light shadow-sm">
            <div class="container wrapper">
                <a class="navbar-brand navbar-collapse" href="{{ url('/') }}">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon">
                    </span>
                </button>

                <div class="navbar navbar-collapse collapse" id="navbarSupportedContent">

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto layer">
                        <!-- Authentication Links -->
                        
                        @if(Auth::user() != null)
                            @if(Auth::user()->type == "ADMIN")
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('film.add')}}">Create A Film</a>
                                </li>
                            @endif
                        @endif

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('film.recent') }}">Films</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('tv') }}">Television</a>
                             </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('games') }}">Games</a>
                            </li> -->
                            @if(Auth::user() != null)
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle"  role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{route('user.account')}}">Account</a>
                                    <a class="dropdown-item" href="{{ route('user.watchlist') }}">WatchList</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @else
                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif

                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @endif
                    </ul>
                </div>
            </div>
        </nav>
</header>