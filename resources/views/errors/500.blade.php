@include('layouts.header')
<?php
    $id = rand(0, 40);
    $url = 'https://s3.eu-central-1.wasabisys.com/lewtube/images/display/backdrop-'.$id.'.png'
?>

<style>
    body{
        height: 100%;
        width: 100%;
        background-image: url( {{ $url }} );
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;   
    }
</style>
<body>

<row>
    <div class="col-xl-3 bg-g" style="height: 100%">
        <h2> Looks Like Something went Wrong!</h2>
        <button class="btn" onclick="history.back()" name="action" type="submit" value="Return">Return</button>
        <a href="{{ route('home') }}"><button class="btn">Home</button></a>
    </div>
</row>

</body>