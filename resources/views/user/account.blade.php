@include('layouts.header')
<script>
    $(document).ready(function(){
        $(".EnablePassword").click(function(){
            $('.PasswordField').slideToggle();
        });
    });
</script>
<div class="container">
    <form class="row"  autocomplete="off">
        <div class="col-xl-3">
            <img width="100%" src="{{ asset('images/user.png') }}">
            <hr>
            <h3>Account Type: <div style="float:right;">{{$currentUser->type}}</h3>
            <h3>Account Status: <div style="float:right;">{{$currentUser->status}}</h3>
        </div>
        <fieldset class="col-xl-9" style="padding:2%">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="name" class="form-control" id="name" style="background-color: white; color:#333"  value='{{ $currentUser->name }}' required autofocus class="name" aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted" >Change this as much as you want!</small>
            </div>
            <br>

            <div class="form-group">
                <label for="lewtube-email">Email address</label>
                                                            <!-- if you see this please don't judge my laziness -->
                <input type="email"  autocomplete="off" class="form-control" style="background-color: white; color:#333" id="lewtube-email" name="lewtube-email" value='{{ $currentUser->email }}' placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">I'll never share your email with anyone else. &#128526; </small>
            </div>
            <br>
            <div class="form-group">
                <button class="EnablePassword" type="button">Change Password</button>
                <div class="PasswordField" style="display: none;">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password"  autocomplete="off" class="form-control" style="background-color: white; color:#333" id="exampleInputPassword1" placeholder="Password">
                    <small id="emailHelp" class="form-text text-muted">All passwords are encrypted, so this field is left blank to respect user privacy. If this field ever autofills know that it was because of your user history not because i have the ability to access the passwords used for this website</small>
                </div>
            </div>
            <button type="submit" style="float:right" class="btn btn-light">Update Account</button>
        </fieldset>
    </form>
    </div>
</div>

@include('layouts.footer')