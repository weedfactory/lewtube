#!/bin/bash

# Ensure the script is run as root or with sudo
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root or with sudo."
    exit 1
fi

# Set the Laravel project directory (adjust this path as needed)
LARAVEL_PROJECT_DIR="../"

# Check if the Laravel project directory exists
if [ ! -d "$LARAVEL_PROJECT_DIR" ]; then
    echo "Laravel project directory not found. Please ensure the path is correct."
    exit 1
fi

# Navigate to the Laravel project directory
cd "$LARAVEL_PROJECT_DIR"

# Check if .env file already exists
if [ -f ".env" ]; then
    echo ".env file already exists. No changes made."
    exit 0
fi

# Create the .env file with default content
cat <<EOL > .env
APP_NAME=Lewtube
APP_ENV=production
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_film
DB_USERNAME=lewtube
DB_PASSWORD=lewtubeRocks420

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

EOL

echo ".env file created successfully in $LARAVEL_PROJECT_DIR."
