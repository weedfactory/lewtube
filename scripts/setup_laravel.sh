#!/bin/bash

# Ensure the script is run as root or with sudo
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root or with sudo."
    exit 1
fi

LARAVEL_PROJECT_DIR="../"

# Check if the Laravel project directory exists
if [ -d "$LARAVEL_PROJECT_DIR" ]; then
    # Navigate to the Laravel project directory
    cd "$LARAVEL_PROJECT_DIR"

    # Install Composer dependencies
    echo "Installing Composer dependencies for Laravel..."
    composer install --optimize-autoloader --no-dev

    # Generate the application key
    echo "Generating application key for Laravel..."
    php artisan key:generate

    echo "Composer dependencies installed and application key generated successfully."
else
    echo "Laravel project directory not found. Please ensure the path is correct."
    exit 1
fi

echo "PHP 7.4, Composer, and Laravel dependencies have been installed successfully."