#!/bin/bash

# Ensure the script is run as root or with sudo
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root or with sudo."
    exit 1
fi

# List of scripts to make executable
SCRIPTS=("setup_php7.4.sh" "create_lewtube_enviroment.sh" "setup_laravel.sh", "setup_nginx_ssh_support.sh")

# Make each script executable
for script in "${SCRIPTS[@]}"; do
    if [ -f "$script" ]; then
        echo "Making $script executable..."
        chmod +x "$script"
    else
        echo "Script $script not found!"
    fi
done

echo "All specified scripts are now executable."

# Optionally, run the scripts
# Uncomment the lines below if you want to run the scripts automatically
# echo "Running setup_php_composer_laravel.sh..."
# ./setup_php_composer_laravel.sh

# echo "Running create_env.sh..."
# ./create_env.sh

echo "Initialization complete."