#!/bin/bash

# Ensure the script is run as root or with sudo
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root or with sudo you idiot!"
    exit 1
fi

# Path to the configuration file
CONFIG_FILE="config.env"

# Check if the configuration file exists
if [ ! -f "$CONFIG_FILE" ]; then
    echo "Configuration file $CONFIG_FILE not found!"
    exit 1
fi

# Source the configuration file
source "$CONFIG_FILE"

# Verify that the environment variables are set
if [ -z "${DB_USERNAME}" ] || [ -z "${DB_PASSWORD}" ] || [ -z "${LARAVEL_PROJECT_DIR}" ]; then
    echo "One or more environment variables are not set properly you stupid shit!!"
    exit 1
fi

# Update package list and install MySQL
echo "Updating package list..."
apt update

echo "Installing MySQL server..."
apt install -y mysql-server

# Start MySQL service
echo "Starting MySQL service..."
systemctl start mysql

# Ensure MySQL starts on boot
echo "Enabling MySQL to start on boot..."
systemctl enable mysql

# Create database and user
echo "Creating MySQL database and user ..."
mysql -u root -e "CREATE DATABASE IF NOT EXISTS db_film;"
mysql -u root -e "CREATE USER IF NOT EXISTS '$DB_USERNAME'@'localhost' IDENTIFIED BY '$DB_PASSWORD';"
mysql -u root -e "GRANT ALL PRIVILEGES ON dbfilm.* TO '$DB_USERNAME'@'localhost';"
mysql -u root -e "FLUSH PRIVILEGES;"

# Import SQL file
SQL_FILE="${LARAVEL_PROJECT_DIR}/dbfilm.sql"
if [ ! -f "$SQL_FILE" ]; then
    echo "SQL file $SQL_FILE not found!"
    exit 1
fi

echo "Importing SQL file $SQL_FILE into dbfilm..."
mysql -u "$DB_USERNAME" -p"$DB_PASSWORD" dbfilm < "$SQL_FILE"

echo "MySQL installation and database import complete."
