-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2024 at 09:49 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbfilm`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors`
--

CREATE TABLE `actors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actors`
--

INSERT INTO `actors` (`id`, `name`) VALUES
(1, 'Robert Deniro'),
(2, 'Toshiro Mifune'),
(3, 'Al Pacino'),
(4, 'Joe Pesci'),
(5, 'Harvey Keitel'),
(6, 'Nicholas Cage'),
(7, 'Kurt Russell'),
(8, 'Willem Dafoe'),
(9, 'Paul Newman'),
(10, 'Tom Cruise'),
(11, 'Harry Dean Stanton'),
(12, 'Winona Ryder'),
(13, 'Michelle Pfeiffer'),
(14, 'Daniel Day Lewis'),
(15, 'Leonardo DiCaprio'),
(16, 'Cate Blanchett');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`id`, `name`) VALUES
(1, 'Martin Scorese'),
(2, 'Andrei Tarkovsky'),
(3, 'Brian DePalma'),
(4, 'Akira Kurosawa'),
(5, 'John Carpenter'),
(6, 'Mike Judge'),
(7, 'Richard Linklater'),
(8, 'Francis Ford Copolla'),
(9, 'Michael Mann'),
(10, 'Walter Hill'),
(11, 'Paul Schrader'),
(12, 'Wim Wenders'),
(13, 'Fred Zinnemann');

-- --------------------------------------------------------

--
-- Table structure for table `episodes`
--

CREATE TABLE `episodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `season` int(11) NOT NULL,
  `episode` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `blurb` varchar(255) NOT NULL,
  `showID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `episodes`
--

INSERT INTO `episodes` (`id`, `season`, `episode`, `name`, `blurb`, `showID`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Smoke Gets in Your Eyes', 'It\'s March 1960. Don Draper, a high-level advertising executive at the Sterling Cooper agency in New York City, struggles to find ideas to keep an account for Lucky Strike cigarettes, while at the same time managing his tangled personal life. Peggy Olson ', 1, NULL, NULL),
(2, 1, 2, 'Ladies Room', 'Don continues to conceal his increasingly complicated personal life, even in the face of Roger Sterling\'s invitation to open up. Meanwhile, Peggy pines for Pete, who is still on his honeymoon, while fending off the advances of several of the men of Sterli', 1, NULL, NULL),
(3, 1, 3, 'Marriage of Figaro', 'Pete returns from his honeymoon, excited about his new marriage but conflicted about his past encounter with Peggy. Don\'s business relationship with client Rachel Menken develops into a personal attraction, while he and Betty host a birthday party for the', 1, NULL, NULL),
(4, 1, 4, 'New Amsterdam', 'Pete faces pressure from his wife, Trudy, regarding a new apartment. He also further alienates Don and almost loses his job at Sterling Cooper by pitching his own copy to a client in a social setting. Bert Cooper advises Don to retain Pete because of his ', 1, NULL, NULL),
(5, 1, 5, '5G', 'Ken gets a short story published in The Atlantic Monthly, inciting envy among his colleagues and driving Pete to ask his wife to meet with an old boyfriend to help Pete get published. Don\'s younger brother Adam, who believed that Don was killed in the Kor', 1, NULL, NULL),
(6, 1, 6, 'Babylon', 'It\'s Mother\'s Day. Roger tries to convince Joan to get her own apartment so they won\'t have to meet in hotels anymore. At a brainstorming session for Belle Jolie lipstick, Peggy has some interesting ideas that cause Freddy Rumsen to suggest she be asked t', 1, NULL, NULL),
(7, 1, 7, 'Red in the Face', 'Roger joins Don for an evening of dinner and drinking, during which Roger makes a pass at Betty, causing a rift in their friendship as the agency prepares for a meeting with members of Richard Nixon\'s campaign staff. Pete faces trouble at home after he ex', 1, NULL, NULL),
(8, 1, 8, 'The Hobo Code', 'As Peggy\'s ad copy proves to be successful, her relationship with Pete becomes more complicated. Don spends the evening with Midge and her Bohemian friends. An encounter Don had as a boy with a hobo is told in flashbacks. Art Director Sal Romano finds him', 1, NULL, NULL),
(9, 1, 9, 'Shoot', 'McCann Erickson, a larger ad agency, tries to hire Don. They offer Betty a modeling job as a part of their attempt to lure him. The agency devises a strategy to help Nixon\'s presidential campaign. Peggy\'s weight gain is noticed in the office, leading to a', 1, NULL, NULL),
(10, 1, 10, 'Long Weekend', 'It\'s Labor Day and after Sterling Cooper loses the Dr. Scholl\'s account, Roger attempts to cheer Don up by arranging for twins to spend the night with them, which results in Roger having a heart attack. Joan goes out for a night out on the town with her r', 1, NULL, NULL),
(11, 1, 11, 'Indian Summer', 'It\'s October and Adam hangs himself after mailing a personal box to Don. Roger has a second heart attack during a meeting with the head of Lucky Strike, Lee Garner, Sr and as a result, Don is given a partnership in Sterling Cooper, making Pete jealous. Pe', 1, NULL, NULL),
(12, 1, 12, 'Nixon vs. Kennedy', 'On November 8, 1960, Sterling Cooper\'s employees have an all-night office party to watch the 1960 United States presidential election results, hoping for Nixon to win against Kennedy. Pete discovers Don\'s real name is Dick Whitman, and Whitman officially ', 1, NULL, NULL),
(13, 1, 13, 'The Wheel', 'At Thanksgiving, Betty discovers her friend\'s husband has been having an affair, leading her to re-evaluate her own marriage. Pete brings in a big account through his father-in-law. Peggy gets promoted to junior copywriter, but without realizing she is pr', 1, NULL, NULL),
(14, 2, 1, 'For Those Who Think Young', 'On Valentine\'s Day 1962, Don and Betty plan a romantic evening in New York, but the evening ends with Don\'s inability to satisfy Betty. Pete gives Trudy a box of Schrafft\'s chocolates for Valentine\'s Day but they have an awkward discussion over Trudy\'s fr', 1, NULL, NULL),
(15, 2, 2, 'Flight 1', 'The crash of American Airlines Flight 1 affects several Sterling Cooper employees, most notably Pete, whose father is killed on the flight, and Duck and Don, who try to manage existing and potential accounts with competing airline companies. Paul and Joan', 1, NULL, NULL),
(16, 2, 3, 'The Benefactor', 'When crude comedian Jimmy Barrett humiliates Utz executives during a commercial shoot, Don has to smooth things over with the company. Harry champions a controversial episode of The Defenders about an abortion; he sees opportunity in sponsoring the show b', 1, NULL, NULL),
(17, 2, 5, 'The New Girl', 'A new secretary named Jane Siegel begins working for Don. Don\'s affair with Jimmy Barrett\'s wife Bobbie leads to Don and Bobbie\'s getting in a car accident and Peggy\'s having to bail them out and cover up the incident.', 1, NULL, NULL),
(18, 2, 6, 'Maidenform', 'Peggy is disappointed that her co-workers are not including her in their business meetings, so Joan offers her advice to try and change that. Duck is made to take care of his dog after his estranged family visits the office.', 1, NULL, NULL),
(19, 2, 7, 'The Gold Violin', 'Don\'s success leads to the purchase of a new car. Sal invites Ken over for Sunday dinner to discuss Ken\'s new short story, which leaves Sal\'s wife feeling left out. Duck feels unappreciated when Don gets all the credit for landing a new account they worke', 1, NULL, NULL),
(20, 2, 8, 'A Night to Remember', 'Because of Peggy\'s work as a copywriter, Father Gill seeks her help promoting a church dance. However, the organizers deem her ideas too controversial. Betty goes to a lot of trouble to help Don set up a dinner for the Heineken beer account (part of a pro', 1, NULL, NULL),
(21, 2, 9, 'Six Month Leave', 'Freddy Rumsen\'s alcoholism results in an embarrassing situation during a pitch meeting with his team, and he is let go from the agency. Roger leaves his wife Mona and takes up with Jane. The death of Marilyn Monroe saddens many of the women in the office.', 1, NULL, NULL),
(22, 2, 10, 'The Inheritance', 'Despite Don and Betty\'s separation, they take a trip together to visit Betty\'s father, who has had a stroke and is in the early stages of dementia. Although the two share an intimate encounter in her father\'s house, Betty tells Don they are still separate', 1, NULL, NULL),
(23, 2, 11, 'The Jet Set', 'Don\'s business trip to Los Angeles takes an unexpected detour when he falls in with a group of wealthy nomads; Peggy attempts to go on a date to a Bob Dylan concert with Kurt before finding out he\'s a homosexual, and Duck holds a secret meeting to help se', 1, NULL, NULL),
(24, 2, 12, 'The Mountain King', 'Don renews his acquaintance with his old friend Anna, the widow of the man whose identity Don stole years before. Pete\'s family problems affect Sterling Cooper\'s account with Clearasil. Joan introduces her fiancé Greg to the employees of Sterling Cooper. ', 1, NULL, NULL),
(25, 2, 13, 'Meditations in an Emergency', 'Don finally returns from his trip to California in the middle of the hysteria generated by the Cuban Missile Crisis. Betty receives the news that she is pregnant, but before reconciling with Don and telling him the news, she engages in a brief sexual enco', 1, NULL, NULL),
(26, 3, 1, 'Out of Town', 'It\'s early 1963. In the aftermath of Sterling Cooper\'s sale to a British company, major changes are made to the staff, including the addition of new employee Lane Pryce and Pete and Ken being named to the same position. Don and Sal both engage in extramar', 1, NULL, NULL),
(27, 3, 2, 'Love Among the Ruins', 'Sterling Cooper argues over the ad campaign for Pepsi\'s new diet cola, Patio. Representatives of Madison Square Garden engage SC in their campaign to demolish Penn Station and build a new MSG. Following concerns over the treatment of Betty\'s ill father, G', 1, NULL, NULL),
(28, 3, 3, 'My Old Kentucky Home', 'A mandatory overtime session leaves Paul, Smitty, and Peggy trying to stave off late-night boredom with cannabis. Roger\'s Kentucky Derby party leads to Don striking up a friendship with a folksy guest named Connie from another event, while Betty meets pol', 1, NULL, NULL),
(29, 3, 4, 'The Arrangements', 'Don crosses paths with his father-in-law, Peggy searches for a new roommate and a new client with money to throw around is very excited about doing business with the firm, though Don wants to make sure it will avoid a conflict with a friend of Cooper\'s. S', 1, NULL, NULL),
(30, 3, 5, 'The Fog', 'In the wake of the death of Betty\'s father, Sally begins to misbehave, much to Betty and Don\'s dismay. Her teacher reports she is troubled by the death of Medgar Evers, which is all over the news. Looks between Don and the teacher reveal stirrings of attr', 1, NULL, NULL),
(31, 3, 6, 'Guy Walks Into an Advertising Agency', 'The agency\'s British owners visit Sterling Cooper to reassign Pryce to one of their India-based companies over the Independence Day weekend. A replacement for Pryce is introduced to the company. Ken brings a riding lawnmower into the office. During a part', 1, NULL, NULL),
(32, 3, 8, 'Souvenir', 'It\'s August 1963. After they win the reservoir case, Betty and Henry cross the line. Pete runs into Joan while out shopping, who now works as a manager of a department store. Don and Betty take a business trip to Rome for Hilton and manage to renew romant', 1, NULL, NULL),
(33, 3, 9, 'Wee Small Hours', 'After a phone call from Hilton, a restless Don drives in to work early and is surprised to see Suzanne jogging along the side of the road so far before sunrise. Lee Garner Jr., an executive for Sterling Cooper\'s largest client Lucky Strike, forces the age', 1, NULL, NULL),
(34, 3, 10, 'The Color Blue', 'Betty discovers Don\'s cache of photographs, revealing his past life. Paul suffers from writer\'s block and grows resentful of Peggy\'s success. The arrival of Suzanne\'s troubled brother complicate her affair with Don, while Pryce is informed that Sterling C', 1, NULL, NULL),
(35, 3, 11, 'The Gypsy and the Hobo', 'As Don is about to leave with Suzanne, Betty confronts him about his identity theft, forcing him to reveal to her the truth about himself. Meanwhile, Roger meets a former client/lover who wishes to rekindle their affair, but Roger tells her he is happy wi', 1, NULL, NULL),
(36, 3, 12, 'The Grown-Ups', 'Roger\'s daughter fears her wedding will be ruined by the news of the Kennedy assassination. The news also affects the others, Peter, already despondent over Ken Cosgrove being promoted over him, resolves to just stay at home and watch the news on TV with ', 1, NULL, NULL),
(37, 3, 13, 'Shut the Door. Have a Seat.', 'Before the sale of Sterling Cooper is to take effect, Roger, Bert, Don and Lane devise a plan to form a new agency, Sterling Cooper Draper Pryce, with them as equal partners. Peggy, Joan, Pete and Harry Crane are recruited to move with them. Betty and Don', 1, NULL, NULL),
(38, 4, 1, 'Public Relations', 'Don\'s secretive demeanor results in an unfavorable interview by an Advertising Age reporter, leading an important client to fire his ad agency. Creative and Don struggle with a bathing suit account for which the client wants to project a wholesome image. ', 1, NULL, NULL),
(39, 4, 2, 'Christmas Comes But Once a Year', 'Don gets a Christmas letter from Sally which highlights his loneliness. Now 18 months sober, Freddy Rumsen turns up at SCDP with a new client, but his old-fashioned prejudice leads to conflict with Peggy. Roger mistakenly invites Lee Garner, Jr. to the fi', 1, NULL, NULL),
(40, 4, 3, 'The Good News', 'Joan is trying to start a family with Greg, but her work schedule and his impending Army commitments make things difficult. Don takes a New Year\'s trip to California to see Anna, and meets her niece Stephanie, who delivers some unsettling news about Anna\'', 1, NULL, NULL),
(41, 4, 4, 'The Rejected', 'It is February 1965. An edict from Roger and Lane puts Pete in a personal dilemma, as he must confront his father-in-law about an account. Pete also finds out from him the news that his wife is pregnant. A focus group for Pond\'s Cold Cream leads to a conf', 1, NULL, NULL),
(42, 4, 5, 'The Chrysanthemum and the Sword', 'Pete enters SCDP into a competition run by Honda, earning the ire of Roger, who, due to his anti-Japanese experiences from World War II, attempts to undercut the other partners\' efforts to win the account. An executive from another agency attempts to posi', 1, NULL, NULL),
(43, 4, 6, 'Waldorf Stories', 'After winning a Clio Award for the Glo-Coat ad, an inebriated Don inadvertently pitches executives from Quaker Oats a slogan for Life cereal that came from Roger\'s wife\'s cousin. Peggy secludes herself in a hotel room with the firm\'s new artistic director', 1, NULL, NULL),
(44, 4, 7, 'The Suitcase', 'An impending deadline leaves the firm in disarray, as Don makes Peggy stay late to work on a Samsonite ad, missing a birthday dinner with her boyfriend. That night, Don receives a call from Anna\'s niece confirming his fears about her health, while an into', 1, NULL, NULL),
(45, 4, 8, 'The Summer Man', 'Don attempts to regain control over his life through physical changes and journal writing. Betty forbids him from attending Eugene\'s birthday party, and is flustered when she and Henry run into Don and Bethany on a date. Don\'s persistence with Faye result', 1, NULL, NULL),
(46, 4, 9, 'The Beautiful Girls', 'Peggy is forced to face some unpleasant facts about a client\'s discriminatory business practices. Don and Faye\'s burgeoning relationship is tested when Sally runs away from home and turns up at the office. Roger tries to rekindle his affair with Joan. Mis', 1, NULL, NULL),
(47, 4, 10, 'Hands and Knees', 'After SCDP lands a contract with North American Aviation, Don and Betty are rattled when FBI agents visit the Francis home as part of the security clearance process. Joan finds out she\'s pregnant with Roger\'s child and considers having an abortion. Lane\'s', 1, NULL, NULL),
(48, 4, 11, 'Chinese Wall', 'The employees of SCDP scramble to hold onto the rest of their accounts when word leaks of Lucky Strike\'s defection to BBDO. Roger lies to the rest of the firm about going to Raleigh to try to win back the account. While waiting for Trudy to give birth to ', 1, NULL, NULL),
(49, 4, 12, 'Blowing Smoke', 'Don runs into his old flame Midge, and learns her life has taken a disturbing turn. After executives from Philip Morris cancel a meeting for potential business, Don has a full-page ad printed in the New York Times announcing the firm will no longer repres', 1, NULL, NULL),
(50, 4, 13, 'Tomorrowland', 'It\'s October 1965. Don hires Megan to accompany him and his children on their trip to California after Betty fires Carla unexpectedly. Don proposes to Megan and she accepts. Peggy spearheads a new campaign for a pantyhose company. Betty and Henry prepare ', 1, NULL, NULL),
(194, 5, 1, 'smoke in your eyes', 'It\'s Memorial Day weekend, 1966, and Don\'s children are spending the holiday with him and his new wife, Megan, at the Drapers\' new and stylish Manhattan apartment. Pete struggles at SCDP due to Roger\'s constant attempts to undermine him through his, Pete\'', 1, NULL, NULL),
(195, 5, 2, 'smoke in your eyes', 'It\'s Memorial Day weekend, 1966, and Don\'s children are spending the holiday with him and his new wife, Megan, at the Drapers\' new and stylish Manhattan apartment. Pete struggles at SCDP due to Roger\'s constant attempts to undermine him through his, Pete\'', 1, NULL, NULL),
(196, 5, 3, 'Tea Leaves', 'When Betty sees a doctor to try to get a prescription for diet pills, the doctor discovers a nodule on her thyroid. Although the nodule is later discovered to be benign, in the meantime Betty dwells on her mortality. Harry and Don go backstage at a Rollin', 1, NULL, NULL),
(197, 5, 4, 'Mystery Date', 'Don runs into an ex-lover, and can\'t seem to escape her presence. Joan\'s husband, Greg, returns from his tour of duty in Vietnam only to reveal that he is being sent back for another year of service. Sally becomes frightened after reading stories on the R', 1, NULL, NULL),
(198, 5, 5, 'Signal 30', 'The episode occurs roughly two weeks after the previous one—around the time of Charles Whitman\'s shooting rampage at the University of Texas at Austin. Pete and Trudy host a dinner party in the suburbs, attended by Don, Megan, Ken, and his wife. Pete also', 1, NULL, NULL),
(199, 5, 6, 'Far Away Places', 'Peggy becomes alienated both at work and in her personal life. Roger and Jane take LSD with a group of intellectuals, altering how they see the world, and allowing them to speak honestly about their marriage. Don and Megan take an impromptu road trip to P', 1, NULL, NULL),
(200, 5, 7, 'At the Codfish Ball', 'Don is honored at a banquet for the American Cancer Society, but finds his professional reputation has been damaged. Megan comes up with a last-minute pitch to save the Heinz account. Sally—whose father discouraged her wearing makeup but told her she\'s a ', 1, NULL, NULL),
(201, 5, 8, 'Lady Lazarus', 'Megan has second thoughts about her career path after the success with Heinz but finds it difficult to tell Don. Peggy, unwittingly caught between the two when Megan\'s lie comes to light, finally lets her frustrations be known to both Megan and Don. Pete ', 1, NULL, NULL),
(202, 5, 9, 'Dark Shadows', 'As a toxic smog cloud hangs over New York City during Thanksgiving 1966, Betty\'s jealousy of Don and Megan leads her to tell Sally about Anna Draper. Don and Peggy are both irritated by Michael Ginsberg\'s rising star, and Don takes a devious approach to c', 1, NULL, NULL),
(203, 5, 10, 'Christmas Waltz', 'Around Christmas, Lane is instructed by his London attorney to wire $8,000 to England within two days for back taxes, or else he risks being arrested. Lane, in fear, forges a check from Don. Harry meets with Paul Kinsey, who has joined the Hare Krishnas a', 1, NULL, NULL),
(204, 5, 11, 'The Other Woman', 'Pete asks Joan to sleep with a client to secure the Jaguar account. Don rejects the idea angrily, but the others approve it without him. The creatives work long nights to come up with the perfect pitch for the presentation. Don becomes furious after learn', 1, NULL, NULL),
(205, 5, 12, 'Commissions and Fees', 'Don discovers that Lane stole money from the company and fires him. Sally has a rendezvous with former neighbor Glen Bishop, which ends abruptly. After Don expresses a yearning for more, Roger gets him a meeting with Dow Chemical, a client that could shap', 1, NULL, NULL),
(206, 5, 13, 'The Phantom', 'Don has a toothache that brings up painful memories of his brother as the firm looks to expand after a profitable quarter with Joan running the books. Roger seeks to expand the affair with Megan\'s mother and asks her to join him on a second LSD trip. Pete', 1, NULL, NULL),
(207, 6, 1, ' ', 'Don and Megan take a vacation to Hawaii for a new client, Sheraton Hotels. Roger receives news of his mother\'s death and at the funeral, his estranged daughter Margaret attempts to cajole Roger into investing in her husband\'s refrigeration business idea. ', 1, NULL, NULL),
(208, 6, 2, ' ', 'Don and Megan take a vacation to Hawaii for a new client, Sheraton Hotels. Roger receives news of his mother\'s death and at the funeral, his estranged daughter Margaret attempts to cajole Roger into investing in her husband\'s refrigeration business idea. ', 1, NULL, NULL),
(209, 6, 3, 'Collaborators', 'Pete and Trudy host a dinner party for their neighbors in Cos Cob. Pete flirts with his neighbor\'s wife, Brenda, and they meet for a tryst in his Manhattan apartment. When Trudy discovers the affair, she kicks Pete out of their home. Megan confides to Syl', 1, NULL, NULL),
(210, 6, 4, 'To Have and to Hold', 'SCDP prepares a pitch for Heinz Ketchup, but is later found out by Heinz Baked Beans and is promptly fired as its advertising firm. Joan tries to fire Scarlett, Harry Crane\'s secretary, as punishment for Scarlett convincing Dawn to commit time card fraud.', 1, NULL, NULL),
(211, 6, 5, 'The Flood', 'On April 4, 1968, Peggy and Megan are up for an advertising award for Heinz Beans when the announcement that Dr. Martin Luther King, Jr. has been assassinated comes in. Work shuts down at Sterling Cooper Draper Pryce as regularly scheduled television prog', 1, NULL, NULL),
(212, 6, 6, 'For Immediate Release', 'Sterling Cooper Draper Pryce prepares an IPO, unbeknownst to Don. Megan\'s mother, Marie, visits and Roger convinces her to join their dinner with Jaguar. At the dinner, Don becomes annoyed with Herb—whom he never truly trusted over Herb\'s condition for th', 1, NULL, NULL),
(213, 6, 7, 'Man with a Plan', 'The newly merged creative team of Sterling Cooper Draper Pryce and Cutler Gleason Chaough brainstorm how they will market margarine for Fleischmann\'s. Don feels threatened by Ted\'s leadership and accepts Sylvia\'s offer for a midday tryst, which causes Don', 1, NULL, NULL),
(214, 6, 8, 'The Crash', 'Don is still upset that his affair with Sylvia has ended and lingers outside her apartment, much to Sylvia\'s annoyance. At the agency, the creative team works through the weekend on the Chevy campaign. While Ted and the CGC creatives attend Frank Gleason\'', 1, NULL, NULL),
(215, 6, 9, 'The Better Half', 'Megan\'s performance as twins on the set of To Have and To Hold gets criticized by the director. Arlene later visits Megan at the apartment, at Megan\'s invitation, to console her. When Megan says she feels lonely, Arlene tries to kiss her but is rebuffed. ', 1, NULL, NULL),
(216, 6, 10, 'A Tale of Two Cities', 'Set against the backdrop of the 1968 Democratic National Convention and ensuing confrontation between police and protestors in Chicago, Don, Roger, and Harry travel to Los Angeles to meet with clients, including Carnation. The three attend a Hollywood par', 1, NULL, NULL),
(217, 6, 11, 'Favors', 'Roger tells Don of a potential new client, Sunkist. They inform Ted prior to their partners\' meeting and Ted loses his temper because he and Jim have been preparing a proposal to land Ocean Spray, which would present a conflict. Sally and her friend, Juli', 1, NULL, NULL),
(218, 6, 12, 'The Quality of Mercy', 'While upland hunting in Michigan with two Chevy executives, Ken Cosgrove is accidentally shot in the side of his face with birdshot. Back in New York, Ken confides to Pete Campbell that he can no longer deal with the personal stress that Chevy is causing ', 1, NULL, NULL),
(219, 6, 13, 'In Care Of', 'Stan tells Don he is offering to relocate to California to support Sunkist and build a west coast presence for the agency \'one desk at a time.\' The agency receives an RFP from Hershey\'s Chocolate and Jim assigns Don to make a pitch. After spending a night', 1, NULL, NULL),
(220, 7, 1, 'Time Zones', 'It is January 1969. On suspension after the loss of the Hershey account, Don visits the West Coast to salvage his marriage to Megan, while secretly feeding Freddy copy. Peggy is at odds with her new boss, Lou Avery, who condescends to the creative time. A', 1, NULL, NULL),
(221, 7, 2, 'A Days Work', 'It is February 1969. Conflict erupts at SC&P on Valentine\'s Day. Pete wins the Southern California Chevrolet dealers\' association as clients, but the other partners, led by Jim, insist that they get approval from the Chevy corporate office. Peggy wrongly ', 1, NULL, NULL),
(222, 7, 3, 'Field Trip', 'It is April 1969. Don flies to Los Angeles, at the request of Megan\'s agent, and inadvertently reveals to Megan that he has been on leave from SC&P since the previous year. Megan, upset at Don\'s deception, reacts badly and throws him out, and he returns t', 1, NULL, NULL),
(223, 7, 4, 'The Monolith', 'Roger learns that his daughter Margaret has run away to a hippie commune upstate, and travels there with Mona to retrieve her. Margaret, now calling herself \'Marigold\', claims to be happy there. Roger unsuccessfully attempts to get Margaret to return for ', 1, NULL, NULL),
(224, 7, 5, 'The Runaways', 'Stan finds a folder of cartoons drawn by Lou, which are roundly mocked by the creative team. Anna\'s niece Stephanie, who is pregnant out of wedlock and running out of money in Los Angeles, calls Don for help. Don asks Megan to let Stephanie stay with her.', 1, NULL, NULL),
(225, 7, 6, 'The Strategy', 'It is June 1969. Bob Benson learns Chevy is dispensing with SC&P\'s services, but that he will soon be offered a job at Buick as a show of appreciation. Bob tells Joan about the offer and proposes marriage to provide him a respectable profile as a family m', 1, NULL, NULL),
(226, 7, 7, 'Waterloo', 'Cutler attempts to have Don fired for breach of contract; Don calls Megan and proposes moving to California should he lose his job at SC&P, but she declines, effectively declaring their marriage over. Don ultimately thwarts Cutler\'s ploy with Roger, Pete ', 1, NULL, NULL),
(227, 7, 8, 'Severance', 'Picking up in April 1970, Don has resumed his womanizing ways as a bachelor. He has a cryptic dream about Rachel Menken and attempts to reconnect with her, only to learn she recently died of leukemia. He also encounters a waitress named Diana at a diner, ', 1, NULL, NULL),
(228, 7, 9, 'New Business', 'Betty reveals to Don that she is pursuing a degree in psychology. Pima Ryan, a famous commercial artist collaborating with SC&P, seduces Stan and unsuccessfully attempts to seduce Peggy. Megan, who is struggling to find work, rejects a sexual proposition ', 1, NULL, NULL),
(229, 7, 10, 'The Forecast', 'It is May 1970. Don sells his apartment. At work, Johnny Mathis fumbles a pitch and seeks Don\'s advice. When Don advises him to make a joke, Mathis\' attempt goes over horribly, and he blames Don for it, lashing out at him. Don fires Mathis. Joan goes on a', 1, NULL, NULL),
(230, 7, 11, 'Time & Life', 'It is June 1970. SC&P learns McCann plans to close their office and move everyone into the parent company\'s headquarters. Don devises a plan to move to California as \'Sterling Cooper West\' and manage the lucrative contracts that conflict with McCann\'s por', 1, NULL, NULL),
(231, 7, 12, 'Lost Horizon', 'At McCann Erickson, Joan is mistreated by McCann\'s chauvinist executives and finds that her accounts are being jeopardized by the carelessness of a McCann colleague. She takes her complaint to Jim Hobart, who offers to buy out her $500,000 stake in the co', 1, NULL, NULL),
(232, 7, 13, 'The Milk and Honey Route', 'Betty learns she has terminal lung cancer. She decides to forgo treatment against Henry and Sally\'s wishes, feeling it will only prolong the inevitable. Duck Phillips provides Pete an opportunity for a career with Learjet in Wichita, Kansas, which would r', 1, NULL, NULL),
(233, 7, 14, 'Person to Person', 'Don learns of Betty\'s diagnosis from Sally. He calls Betty and implores her to let the children stay with him, but Betty insists on leaving them with her brother and his wife. Meanwhile, Ken enlists Joan with producing a script for an industry film by Dow', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `about` varchar(400) NOT NULL,
  `director` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `title`, `about`, `director`, `year`, `created_at`, `updated_at`) VALUES
(2, 'Mean Streets', 'Scorseses breakout feature film, taken from events and stories he heard growing up, Harvey Keitel and Robert DeNiro play to young hoodlums on the streets on the new york in this slice of life about how financial instability can strain a friendship', '1', 1973, NULL, NULL),
(3, 'Scarface', 'Tony Montana a Cuban immigrant arrives at a refugee camp in Miami. Coming with homes of acquiring the American Dream, Tony\'s pursuit and lust for power will make him a legend but it is hard to stay at the top.', '3', 1983, NULL, '2024-09-22 13:08:01'),
(4, 'Seven Samurai', 'A group of mountain bandits threatens their return to destroy a small feudal village who muster all they can to find seven samurai to protect their village', '4', 1954, '2024-09-11 18:51:03', '2024-09-22 15:30:47'),
(5, 'Andrei Rublev', 'Famous Russian artist Andrei Rublev takes a vow of silence in response to the question of how such injustices like the bruatility of war which ravages the 16th century Russian backdrop', '2', 1966, NULL, NULL),
(6, 'Taxi Driver', 'Travis Bickle a lonely man with a lonely mind get a job working late nights in a crime invested 70s New York, his mind polluted by the corruption he sees and his body by his inability to take care of himself.', '1', 1976, '2024-09-09 23:11:33', '2024-09-22 13:01:27'),
(8, 'Slacker', 'A series of intertwined shorts on a single shot cataloging the day of a sleepy town.', '7', 1990, '2024-09-11 18:52:51', '2024-09-11 18:52:51'),
(9, 'The Thing', 'When a strange dog shops up at this arctic research facility, things descend into chaos with as friend is pit against friend', '5', 1982, '2024-09-11 20:54:51', '2024-09-11 20:54:51'),
(10, 'Beavis and Butthead Do America', 'The boys worry that they\'ll never score and some stuff happens', '6', 1996, '2024-09-11 21:08:03', '2024-09-22 13:54:00'),
(16, 'The Warriors', 'With New York city gang rivalry\'s heating up The Warriors representing Cony Island travel to Manhattan for a meet with all the gangs about forming a truce. Things turn south and all eyes are on The Warriors - can these boys make it home? ', '10', 1979, '2024-09-11 23:42:26', '2024-09-11 23:42:26'),
(17, 'Who\'s That Knocking At My Door', 'Scorese\'s directorial debut, a slice of Italian American life that he romanticizes throughout his career. J.R is a young guy causing trouble with his friends but freque ntly finds his thoughts drifting to a girl has been courting.', '1', 1967, '2024-09-22 09:29:05', '2024-09-22 13:53:32'),
(18, 'Boxcar Bertha', 'I watched this movie at some point and i literally can\'t remember a single thing that happened it tbh. There is a lot of Martin Scorsese\'s movies on this website you should watch something else.', '1', 1972, '2024-09-22 09:41:21', '2024-09-22 12:14:48'),
(19, 'After Hours', 'A word processor who vaguely finds his job interesting meets a girl in a late night diner; in an attempt to bone he tries to meet this girl and again and boy does he regret it!', '1', 1985, '2024-09-22 09:48:27', '2024-09-22 11:29:35'),
(20, 'Raging Bull', 'A biography of the life of professional boxer Jake LaMotta, set over several decades from an 1940s Italian American backdrop Raging Bull depicts the rise and fall of LaMotta as well as his sexual and physical inadequacies that leave him isolated; alienating those he cares most for.', '1', 1980, '2024-09-22 10:20:31', '2024-09-22 12:43:58'),
(21, 'Bringing Out the Dead', 'A sleep deprived Nick Cage slowly loses his grasp on sanity as he handles night shifts for the New York ambulance corp.', '1', 1999, '2024-09-22 10:21:26', '2024-09-22 13:52:59'),
(22, 'The Last Temptation of Christ', 'In this reimagination of the protestant life of Christ. A Tale of Christ the man afflicted by the voices in his head the legacy that is cast upon him. While trying to address', '1', 1988, '2024-09-22 13:44:56', '2024-09-22 13:52:35'),
(23, 'The King of Comedy', 'Rupert Pupkin desperately wants to become a comedian and approaches talk show host Jerry Langford for a chance to perform on his show. When all fails, he teams up with Masha to kidnap Jerry.', '1', 1982, '2024-09-22 13:48:47', '2024-09-22 13:52:09'),
(24, 'Cape Fear', 'When a US attorney starts to notice strange activities happening in his life he realizes that murder he helped get convicted is finally out and is seeking revenge', '1', 1991, '2024-09-22 14:35:13', '2024-09-22 14:35:43'),
(25, 'New York New York', 'i actually haven\'t seen this so idk', '1', 1977, '2024-09-22 14:36:35', '2024-09-22 14:36:50'),
(26, 'The Colour of Money', 'A sequel to the hustler, Tom Cruise and his girl meet Fast Eddie whilst trying to hussle him out of his money in pool. Has Eddie learned enough to teach this upstart the error of his ways before he falls down a similar path?', '1', 1986, '2024-09-22 14:46:09', '2024-09-22 14:46:09'),
(27, 'Paris Texas', 'Discovered after being missing for 4 years Harry Dean Stanton in small Texian town in the desert, with nothing on him but a picture of a vacant lot in Paris Texas', '12', 1984, '2024-09-22 16:11:20', '2024-09-22 16:11:20'),
(28, 'Kundun', 'When the Chinese communist forces evade Tibet, the 14th Dalai Lama comes to India in exile. With the communists threatening to execute him, it seems impossible for him to return to his homeland.', '1', 1997, '2024-09-22 16:17:29', '2024-09-22 16:17:37'),
(29, 'Alice Doesn\'t Live Here Anymore', 'A recently widowed woman attempts to navigate her way through her life while raising her son as a single mother', '1', 1974, '2024-09-22 16:21:21', '2024-09-22 16:24:20'),
(30, 'Goodfellas', 'the biography of Henry Hill a low level mob member and his Icarian rise and fall and that of his friends Joe Pesci and Bobby DeNiro', '1', 1990, '2024-09-22 16:34:41', '2024-09-22 16:35:02'),
(31, 'Age of Innocence', 'a melodrama about nothing that really madly happens but the protagonist without realizing is an idiot on which the plot falls on to him. - craig', '1', 1993, '2024-09-22 16:39:20', '2024-09-22 16:39:20'),
(32, 'Gangs of New York', 'Leo\'s father that is killed in a gang risen mob to improve the quality of their', '1', 2002, '2024-09-22 16:41:27', '2024-09-22 16:43:26'),
(33, 'Casino', 'Ace Rothstein after making great money for the New York mob families is asked to take over the Vegas Casino circuit and his good friend Nicky Santoro who has plans to use this new age of trafficking to prove his value to the big families through', '1', 1995, '2024-09-22 17:18:48', '2024-09-22 17:18:48'),
(34, 'Aviator', 'Leo plays the guy made planes and his broad of a wife who\'s knock out! He suffers with the affliction and it makes it hard to go outside, can he get over that?', '1', 2004, '2024-09-22 17:21:02', '2024-09-22 17:21:15'),
(35, 'A Man For Every Season', 'In Liu of Henry VIII seeking to divorce his first wife and remarry he degrees that the British clergy to sign a letter of consent to this action. Thomas More won\'t refuse to sign but won\'t agree with it either, making a person of much criticism.', '13', 1966, '2024-09-22 17:42:55', '2024-09-22 17:43:03');

-- --------------------------------------------------------

--
-- Table structure for table `film__actors`
--

CREATE TABLE `film__actors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `film` int(11) NOT NULL,
  `actor` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `film__actors`
--

INSERT INTO `film__actors` (`id`, `film`, `actor`, `created_at`, `updated_at`) VALUES
(1, 9, 2, NULL, NULL),
(8, 17, 5, NULL, NULL),
(9, 20, 1, NULL, NULL),
(10, 20, 4, NULL, NULL),
(13, 21, 6, NULL, NULL),
(14, 6, 1, NULL, NULL),
(17, 3, 3, NULL, NULL),
(18, 22, 5, NULL, NULL),
(19, 22, 8, NULL, NULL),
(20, 23, 1, NULL, NULL),
(21, 24, 1, NULL, NULL),
(22, 25, 1, NULL, NULL),
(23, 26, 9, NULL, NULL),
(24, 26, 10, NULL, NULL),
(25, 27, 11, NULL, NULL),
(26, 29, 5, NULL, NULL),
(27, 30, 1, NULL, NULL),
(28, 30, 4, NULL, NULL),
(29, 31, 12, NULL, NULL),
(30, 31, 13, NULL, NULL),
(31, 31, 14, NULL, NULL),
(32, 32, 14, NULL, NULL),
(33, 32, 15, NULL, NULL),
(34, 32, 14, NULL, NULL),
(35, 32, 15, NULL, NULL),
(36, 33, 1, NULL, NULL),
(37, 33, 3, NULL, NULL),
(38, 34, 15, NULL, NULL),
(39, 34, 16, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `film__genres`
--

CREATE TABLE `film__genres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `film` int(11) NOT NULL,
  `genre` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `film__genres`
--

INSERT INTO `film__genres` (`id`, `film`, `genre`, `created_at`, `updated_at`) VALUES
(2, 17, 4, NULL, NULL),
(3, 18, 5, NULL, NULL),
(4, 19, 6, NULL, NULL),
(5, 19, 7, NULL, NULL),
(6, 19, 8, NULL, NULL),
(7, 19, 9, NULL, NULL),
(8, 20, 4, NULL, NULL),
(9, 20, 10, NULL, NULL),
(10, 21, 12, NULL, NULL),
(11, 21, 13, NULL, NULL),
(12, 3, 1, NULL, NULL),
(13, 3, 4, NULL, NULL),
(16, 22, 10, NULL, NULL),
(17, 22, 4, NULL, NULL),
(18, 23, 4, NULL, NULL),
(19, 23, 9, NULL, NULL),
(20, 24, 13, NULL, NULL),
(21, 26, 4, NULL, NULL),
(22, 27, 4, NULL, NULL),
(23, 28, 15, NULL, NULL),
(24, 29, 4, NULL, NULL),
(25, 30, 16, NULL, NULL),
(26, 30, 10, NULL, NULL),
(27, 31, 4, NULL, NULL),
(28, 34, 10, NULL, NULL),
(29, 35, 4, NULL, NULL),
(30, 35, 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'action', NULL, NULL),
(2, 'western', NULL, NULL),
(3, 'samurai', NULL, NULL),
(4, 'drama', NULL, NULL),
(5, 'Blaxploitation', NULL, NULL),
(6, 'Slice of Life', NULL, NULL),
(7, 'noir', NULL, NULL),
(8, 'Yuppie', NULL, NULL),
(9, 'Comedy', NULL, NULL),
(10, 'Biography', NULL, NULL),
(11, 'Nicholas Cage', NULL, NULL),
(12, 'Dreamy', NULL, NULL),
(13, 'Horror', NULL, NULL),
(15, 'Religious', NULL, NULL),
(16, 'Crime', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_01_06_200447_create_films_table', 1),
(6, '2022_01_06_202439_create_directors_table', 1),
(7, '2022_01_06_202705_create_episodes_table', 1),
(8, '2022_01_06_202925_create_genres_table', 1),
(9, '2022_01_06_203100_create_ratings_table', 1),
(10, '2022_01_06_203200_create_reviws_table', 1),
(11, '2022_01_06_203206_create_reviews_table', 1),
(12, '2022_01_06_203446_create_suggestions_table', 1),
(13, '2022_01_06_203559_create_televisions_table', 1),
(14, '2022_01_06_203744_create_video__timestamps_table', 1),
(15, '2022_01_07_130105_create_actors_table', 1),
(16, '2022_01_08_200434_create_film__genres_table', 1),
(17, '2022_01_08_200543_create_film__actors_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` int(11) NOT NULL,
  `movie` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `user`, `movie`, `rating`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 1, '2024-09-09 23:17:59', '2024-09-09 23:17:59'),
(2, 1, 4, 1, '2024-09-14 12:37:31', '2024-09-14 12:37:31');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` int(11) NOT NULL,
  `movie` int(11) NOT NULL,
  `content` varchar(600) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user`, `movie`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'yea i think this film is alright its a bit long tho.. Have you seen Magnificent Seven? thats got charles bronson! i think this film is a good example of why age ratings should be in movie, a lot of time is wasted with toshiro mifune being silly', NULL, NULL),
(2, 1, 21, 'Taxi Driver but Nick Cage', '2024-09-22 11:46:56', '2024-09-22 12:47:58'),
(4, 1, 20, 'some of my favourite editing ever, good 4 you Thelma!', '2024-09-22 11:48:54', '2024-09-22 11:57:06'),
(5, 1, 17, 'i really think there is a lot to like about this film! i find the The Door\'s sex scenes are actually a bit much but martie thought the same thing and didn\'t actually want them in the film.', '2024-09-22 11:53:07', '2024-09-22 11:53:07'),
(6, 1, 3, 'I do actually think is in an incredible example of the fantasy of the capitalist ideal, \"The world is yours\" blimp is actually one of my favourite stills from a film ever. Betrayed and reviled by his own family, Tony will stop at nothing  to prove that his individuality was worth all he has sacrificed - something we forget with the luxuries of being born in a world where one can\'t die for a purpose.', '2024-09-22 13:07:11', '2024-09-22 13:07:11'),
(7, 1, 26, 'i always thought the choice to play nine-ball instead of ten-ball pool is strange, don\'t think any of those lads could win a game in the windmill.', '2024-09-22 14:46:10', '2024-09-22 14:46:10'),
(8, 1, 28, 'Kundun! i liked it!', '2024-09-22 16:17:29', '2024-09-22 16:17:29'),
(9, 1, 30, 'its a good one!', '2024-09-22 16:34:41', '2024-09-22 16:34:41'),
(10, 1, 32, 'i\'ve definitely rented as a ps2 game at some stage', '2024-09-22 16:41:27', '2024-09-22 16:41:27'),
(11, 1, 33, 'Robert DeNiro is very based', '2024-09-22 17:18:49', '2024-09-22 17:18:49'),
(12, 1, 34, 'age of the future', '2024-09-22 17:21:02', '2024-09-22 17:21:02');

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `televisions`
--

CREATE TABLE `televisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `about` varchar(1000) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `televisions`
--

INSERT INTO `televisions` (`id`, `title`, `about`, `created_at`, `updated_at`) VALUES
(1, 'Mad Men', 'Don Draper is the creative director of a manhattan advertising agency, while having everything he could ever want he he has nothing. Set during the american cultural revolution of the 60s, Mad Men follows the staff of Sterling Cooper attempting to balance artistic integrity with consumer advertising while trying to keep the spectres of extensial dread at bay.', NULL, NULL),
(2, 'Father Ted', 'Three misfit priests and their housekeeper live on Craggy Island, not the peaceful and quiet part of Ireland that it seems to be.', NULL, NULL),
(3, 'Beavis & Butthead', 'Mike Judge narrates  MTV\'s 90s and 2000s music videos as two horny teenage idiots.', NULL, NULL),
(4, 'Chernobyl', 'A mini series cataloging the heroic effort of everyone who tried to save the world!', NULL, NULL),
(5, 'Nathan For You', 'Nathan Fielder graduated from business school with really good grades now he\'s trying to help failing business from shutting down', NULL, NULL),
(6, 'Day Today', 'Chris Morris, Steve Coogan and many others come together to make a late night satircal news show', NULL, NULL),
(7, 'Twin Peaks', 'In a sleepy forest town the teenage prom queen, Laura Palmer is found dead, wrapped in plastic. FBI agent Dale Cooper is sent to investigate.', NULL, NULL),
(8, 'The Sopranos', 'Tony Soprano the head of the diMeo crime family. When he collapses he starts to see a therapist who can but he can\'t discuss his business.', NULL, NULL),
(9, 'Knowing Me Knowing You', 'Alan Partridge hosts a throve colourful locals yet fucks everything up', NULL, NULL),
(10, 'Seinfeld', 'A show about nothing.', NULL, NULL),
(11, 'The Wire', 'When the Baltimore police department start to realise that a unknown criminal by the name of Avon Barksdale has suddenly became the head of drug trade in all of West Balitmore they set up a special task force of some cool lads to investigate this', NULL, NULL),
(12, 'Tales From The Tour Bus', 'Mike Judge introduces a history of musician each season a different genre', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `type`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Lewis', 'lewiskerinbrowne1@gmail.com', NULL, '$2y$10$.rAZKaNL.J5D0XZQV1uMieUCP0LKzBTlt6fIOrNsHVV1Ix717gKcK', 'ADMIN', 'ACTIVE', NULL, NULL, NULL),
(3, 'Anonymous User', 'Anonymous ', NULL, '$2y$10$.rAZKaNL.J5D0XZQV1uMieUCP0LKzBTlt6fIOrNsHVV1Ix717gKcK', 'ANON', 'ACTIVE', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video__timestamps`
--

CREATE TABLE `video__timestamps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` int(11) NOT NULL,
  `film` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `episodes`
--
ALTER TABLE `episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `film__actors`
--
ALTER TABLE `film__actors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `film__genres`
--
ALTER TABLE `film__genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `televisions`
--
ALTER TABLE `televisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video__timestamps`
--
ALTER TABLE `video__timestamps`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actors`
--
ALTER TABLE `actors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `episodes`
--
ALTER TABLE `episodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `film__actors`
--
ALTER TABLE `film__actors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `film__genres`
--
ALTER TABLE `film__genres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `televisions`
--
ALTER TABLE `televisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `video__timestamps`
--
ALTER TABLE `video__timestamps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
