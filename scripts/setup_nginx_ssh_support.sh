#!/bin/bash

# Ensure the script is run as root or with sudo
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root or with sudo."
    exit 1
fi

# Path to the configuration file
CONFIG_FILE="config.env"

# Check if the configuration file exists
if [ ! -f "$CONFIG_FILE" ]; then
    echo "Configuration file $CONFIG_FILE not found!"
    exit 1
fi

# Source the configuration file
source "$CONFIG_FILE"

# Verify that the environment variables are set
if [ -z "${LARAVEL_PROJECT_DIR}" ]; then
    echo "Environment variable LARAVEL_PROJECT_DIR is not set properly."
    exit 1
fi

# Install Nginx
echo "Installing Nginx..."
apt update
apt install -y nginx

# Enable Nginx to start on boot and start the service
echo "Enabling and starting Nginx service..."
systemctl enable nginx
systemctl start nginx

# Install Certbot for Let's Encrypt SSL
echo "Installing Certbot..."
apt install -y certbot python3-certbot-nginx

# Create Nginx server block for iloveamberleaf.com
echo "Creating Nginx server block for iloveamberleaf.com..."
cat <<EOL > /etc/nginx/sites-available/iloveamberleaf.com
server {
    listen 80;
    server_name iloveamberleaf.com www.iloveamberleaf.com;

    root /var/www/html/lewtube/public;
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOL

# Create Nginx server block for VPS API at IP 57.128.170.15
echo "Creating Nginx server block for VPS API at IP 57.128.170.15..."
cat <<EOL > /etc/nginx/sites-available/57.128.170.15
server {
    listen 80;
    server_name 57.128.170.15;

    root /var/www/html/lewtube/public;
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOL

# Enable the sites by creating symbolic links
echo "Enabling Nginx server blocks..."
ln -s /etc/nginx/sites-available/iloveamberleaf.com /etc/nginx/sites-enabled/
ln -s /etc/nginx/sites-available/57.128.170.15 /etc/nginx/sites-enabled/

# Test Nginx configuration
echo "Testing Nginx configuration..."
nginx -t

# Reload Nginx to apply changes
echo "Reloading Nginx..."
systemctl reload nginx

sudo snap install certbot 

# Obtain SSL certificates for iloveamberleaf.com using Certbot
echo "Obtaining SSL certificate for iloveamberleaf.com using Certbot..."
certbot --nginx -d iloveamberleaf.com -d www.iloveamberleaf.com --non-interactive --agree-tos -m lewiskerinbrowne1@gmail.com

# Test automatic renewal
echo "Testing Certbot automatic renewal..."
certbot renew --dry-run

echo "Nginx setup and SSL configuration complete."
