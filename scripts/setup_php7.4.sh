#!/bin/bash

# Ensure the script is run as root or with sudo
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root or with sudo."
    exit 1
fi

# Update package list
echo "Updating package list..."
apt update

# Install software-properties-common to manage apt repositories
echo "Installing software-properties-common..."
apt install -y software-properties-common

# Add PHP repository (Ondrej's PPA for PHP)
echo "Adding PHP repository..."
add-apt-repository ppa:ondrej/php -y
apt update

# Install PHP 7.4 and required extensions
echo "Installing PHP 7.4 and required extensions..."
apt install -y php7.4 php7.4-cli php7.4-fpm php7.4-mbstring php7.4-xml php7.4-bcmath php7.4-json php7.4-mysql php7.4-zip php7.4-tokenizer php7.4-curl php7.4-common php7.4-gd php7.4-intl php7.4-soap php7.4-ldap

# Verify PHP installation
echo "PHP version:"
php -v

# Download and install Composer globally
echo "Installing Composer..."
EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php

# Verify Composer installation
echo "Composer version:"
composer -V

echo "PHP 7.4 and Composer have been installed successfully."
